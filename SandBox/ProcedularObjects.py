'''
Created on 20/02/2016

@author: Rafal Starzyk
'''

import cmath
import math
from panda3d.core import TextNode, GeomNode, GeomVertexFormat, GeomVertexData, Geom, \
    GeomVertexRewriter, GeomTristrips, Vec3, \
    GeomTrifans, Quat, GeomPoints, GeomLinestrips, PNMImage, \
    GeomTriangles, Texture
import random
import time

import ErosionComputationGeometries as ECG
from Flow import FluidTriFlow, SedimentTriFlow, SedimentEdgeFlow, \
    FluidEdgeFlow
import FlowCalculator as FC
from Globals import Ve0, EROSION_DEPTH_FACTOR, REF, RIEF, FSVEF, BEDROCK_DEPTH, MASS_PER_VOLUME_FLUID, MIN_POINT_HEIGHT_DIFFERENCE
import Tools as T
import cProfile, pstats, StringIO
import Drainage as D
from math import floor
from collections import namedtuple
from ErosionComputationGeometries import Triangle

N1 = 2 #1.7
N2 = 0.5
random.seed(0)
ug = Vec3(0,0,-1)
numErosionIt = 3
numInitialIt = 3

class Object:
    def __init__(self, world, numIterations, law, heightVariability, roughness, colors, randomWeights):
        '''
        Constructor
        '''
        self.world = world
        self.numIterations = int(numIterations)
        self.law = law

        self.heightVariability = float(heightVariability)
        self.initHeightVariability = float(heightVariability)
        self.roughness = float(roughness)
        self.colors = colors
        self.randomWeights = randomWeights
        self.heights = []
        self.maxHeight = 0.0
        self.minHeight = float("inf")
        self.heightRange = 1.0
        
        self._visualisations = {}
        self._activeVisu = None
        self._dataWriters = {}
        
        self._erosionIterations = numErosionIt
        self.cumulativeNumIterations = (numIterations) * numErosionIt
        self.currentIteration = 0
        
    def generateHeights(self, usePoints = False):
        # Update parameters
                
        self.heightVariability /= self.roughness
                    
        
        # Interpolate n-1 numbers from n numbers in each row
        for iR in range(len(self.heights)):
            if len(self.heights[iR]) > 1:
                if usePoints:
                    heightsList = map(lambda p: p.position.getZ(), self.meshData["points"][iR])
                else:
                    heightsList = self.heights[iR]
                self.heights[iR] = self.interpolateHeights(heightsList)
        
        # Interpolate m-1 new rows of numbers from m rows
        self.heights = self.interpolateRows(usePoints)

    def generateHeight(self, meanHeight):
        # Generate random height based on the mean value
        if self.law == "triangular":
            height = random.triangular( meanHeight - self.heightVariability,
                                        meanHeight + self.heightVariability,
#                                         meanHeight - self.heightVariability*(2*random.random()-1)
                                        )
        elif self.law == "normal":
#             height = random.gauss(meanHeight, self.heightVariability)
            if meanHeight < 0:
                height = random.gauss(meanHeight, self.heightVariability**N1)
            else:
                factor = N1 - meanHeight*(N1-N2)/max(self.maxHeight, 10)
                height = random.gauss(meanHeight, self.heightVariability**factor)
        elif self.law == "uniform":
            height = random.uniform(meanHeight - self.heightVariability,
                                    meanHeight + self.heightVariability)
        else:
            raise Exception("Unknown probability law")
                    
        self.maxHeight = max(height, self.maxHeight)
        self.minHeight = min(height, self.minHeight)
        
        return height
    
    def interpolateHeights(self, heightList):
        newHeightsList = []
        length = len(heightList)-1
        # Circular lists need one more interpolation
        if isinstance(heightList, T.CircularList):
            length = len(heightList)
            
        for i in range(length):
            # Interpolate between two heights
            if self.randomWeights == True:
                noise = random.random()
                meanHeight = (noise*heightList[i] + (1-noise)*heightList[i+1])
            else:
                meanHeight = (heightList[i] + heightList[i+1])/2
            height = self.generateHeight(meanHeight)
            newHeightsList.append(height)
            
        return T.createAlternatingList( heightList, newHeightsList)
    
    def interpolateRows(self, usePoints):
        newRowList = []
        
        numNewRows = len(self.heights)-1
            
        for i in range(numNewRows):
            # Interpolate between two rows
#             if usePoints:
#                 heightsList1 = map(lambda p: p.position.getZ(), self.meshData["points"][i])
#                 heightsList2 = map(lambda p: p.position.getZ(), self.meshData["points"][i+1])
#             else:
#                 heightsList1 = self.heights[i]
#                 heightsList2 = self.heights[i+1]
            newRow = self.addRow(self.heights[i], self.heights[i+1])
            newRowList.append(newRow)
            
        return T.createAlternatingList( self.heights, newRowList)   
        
    def getColor(self, value):
        for idx in range(len(self.colors)):
            if value <= self.colors[idx]["normHeight"]:
                break
             
        factor = self.colors[idx]["normHeight"] - self.colors[idx-1]["normHeight"]
        offset = self.colors[idx-1]["normHeight"]
        trsfH = (value - offset)/factor
        color1 = self.colors[idx-1]["+value"]
        color2 = self.colors[idx]["-value"]
        
        return color1*(1 - trsfH) + color2*trsfH
        
    def getGeomIteration(self, cumIt):
        if cumIt == self.cumulativeNumIterations: return self.numIterations-1
        else: return cumIt // self._erosionIterations
        
    def getEroIteration(self, cumIt):
        if cumIt == self.cumulativeNumIterations: return self._erosionIterations
        else: return cumIt % self._erosionIterations
    
    def writeElevationDatum(self, cumIt, type, h, p3, h3):
        color = self.getColor(h)
        self._dataWriters[cumIt][type]["color"].addData3f( color )
        
        for i in range(len(self.colors)):
            if h <= self.colors[i]["normHeight"]:
                break
        if self.colors[i-1]["type"] == "flat":
            self._dataWriters[cumIt][type]["vertex"].addData3f( p3 )
        elif self.colors[i-1]["type"] == "elevated":
            self._dataWriters[cumIt][type]["vertex"].addData3f( p3 + h3 )
        
        return
        
class Section(Object):
    '''
    classdocs
    '''
    
    def __init__(self, world, length, numIterations, law, heightVariability, roughness, colors, randomWeights):
        '''
        Constructor
        '''
        Object.__init__(self, world, numIterations, law, heightVariability, roughness, colors, randomWeights)
        # A minimal heights matrix is 2x2
#         self.heights = [[ 0, 0, 0],
#                         [ 0, 10, 0],
#                         [ 0, 0,  0]]
        self.heights = [[ 10, 6, 10],
                        [ 7, 3, 7],
                        [ 4, 0, 4]]
#         self.heights = [[ 10, 0, 10],
#                         [ 0, 10, 0],
#                         [ 10, 0, 10]]
#         self.heights = [[ 10, 10],
#                         [ 0, 0]]
#         self.heights = [[ 0, 0],
#                         [ 0, 0]]
#         self.heights = [[ 10, 10, 10],
#                         [ -10, -10, -10],
#                         [ -10, -10, -10]]
#         self.heights = [[ 10, 10, 10],
#                         [ 0, 0, 0],
#                         [ 10, 10, 10]]
#         self.heights = [[-10,  10, 10],
#                         [ 10, -10, 10],
#                         [ 10,  10, 10]]
#         self.heights = [[ 1, 1, 1, 1, 1],
#                         [ 1, 0, -1, 0, 1],
#                         [ 1, 0, 0,  0, 1],
#                         [ 1, 0, -1, 0, 1],
#                         [ 1, 1, 1, 1, 1]]
        self.heights = [[h*1 for h in hs] for hs in self.heights]
        
        self.length = length
        self.getNumPoints = lambda i: 2**i*(len(self.heights) - 1) + 1
        self.numPoints = self.getNumPoints(numInitialIt + numIterations)
#         self.unitLength = length/(2**(numInitialIt + numIterations+len(self.heights)-2))
        self.showSedimentTransport = True
        self.showDrainageBasins = False
        self.labelType = "basinId"
        self.textNodes = []
        self.meshData = {'points': [], 'edges': {}, 'triangles': {}}
        self._pointCounter = 0
        
        heightsStartTime = time.time()
        for it in range(numInitialIt):
            self.generateHeights()
        print "Calculated heights in", time.time() - heightsStartTime, "seconds."
        
        self.generateMeshData(0)
        
        for it in range(1, numIterations+1):
            for erit in range(1, self._erosionIterations+1):
                self.currentIteration = (it-1) * self._erosionIterations + (erit-1)
                print "Defining drainage basins" + str(self.currentIteration)
                self.defineDrainageBasins()
                print "Fluid calculation " + str(self.currentIteration)
                FC.calculateFluidFlows(self.meshData, it-1, self.currentIteration == self.cumulativeNumIterations-1)
                print "Sediment transport calculation " + str(self.currentIteration)
                self.transportSediment(it-1)
                # Update point positions
                self._updateGeometry()
                
            if self.currentIteration >= self.cumulativeNumIterations-1: break
            self.generateHeights(True)
            self.generateMeshData(it, True)
        
        if self._erosionIterations > 0:
            self.world.app.addErosionSlider(self.cumulativeNumIterations)
            
        self._updateHeightBounds()
        self._updateFlowBounds()
        
        for cumIt in range(self.cumulativeNumIterations):
            self.showVisualisation(cumIt, "elevation")
            
        print "Finished in", time.time() - heightsStartTime, "seconds."
        
        if T.lineIntersectSegment.numCalls > 0:
            print "PERCENT FAILURES = ", 100. * float(T.lineIntersectSegment.failures) / float(T.lineIntersectSegment.numCalls), " %"
        
    def _updateGeometry(self):
        # Updates the geometrical data of mesh elements after the position of some points changed
        geomTime = time.time()
        trisToUpdate = set()
        edgesToUpdate = set()
        # Update points
        for basinId, bottom in D.basinBottoms.iteritems():
            if basinId == 3154:
                pass
            sp = D.UpdateGeomSearchProblem(bottom, self.minHeightDifference, basinId)
            T.uniformCostSearch(sp)
            trisToUpdate.update(sp.trisToUpdate)
            edgesToUpdate.update(sp.edgesToUpdate)
            
        for entrance in D.externalBasinEntraces:
            sp = D.UpdateGeomSearchProblem(entrance, self.minHeightDifference)
            T.uniformCostSearch(sp)
            trisToUpdate.update(sp.trisToUpdate)
            edgesToUpdate.update(sp.edgesToUpdate)
            
        # Update triangles
        for triangle in trisToUpdate:
            triangle.updateGeometry()
        
        # Update edges
        for edge in edgesToUpdate:
            edge.updateGeometry()
        
        for row in self.meshData["points"]:
            for point in row:
                point.updateLowestNeighbor()
                point.heights.append(point.position.getZ())
#                 point.hasEroded = False
        print "Updated geometry in", time.time() - geomTime, "seconds."
                
    def _updateHeightBounds(self):
        self.maxHeight = 0.0
        self.minHeight = float("inf")
        self.heightRange = 1.0
        
        numRows = len(self.meshData["points"])
        for iR in range(numRows):
            for iH in range(numRows):
                point = self.meshData["points"][iR][iH]
                for i in range(len(point.heights)):
                    self.maxHeight = max(point.heights[i], self.maxHeight)
                    self.minHeight = min(point.heights[i], self.minHeight)
                
        self.heightRange = self.maxHeight - self.minHeight
        
    def _updateFlowBounds(self):
        self.minVelocity = 0.0
        self.maxVelocity = 0.0
        
        self.minFlow = 0.0 #float("inf")
        self.maxFlow = 0.0
        
        # Calculate extrema
        for it in self.meshData["triangles"]:
            numRows = len(self.meshData["triangles"][it])
            for iR in range(numRows):
                row = self.meshData["triangles"][it][iR]
                for tri in row:
                    if len(tri.avgFlowVelocities) == 0:  continue
                    for eroIt in range(len(tri.avgFlowVelocities)):
                        self.maxVelocity = max(tri.avgFlowVelocities[eroIt], self.maxVelocity)
                        self.maxFlow  = max(tri.flows[eroIt], self.maxFlow)
                    
#         print "Max speed = ", self.maxVelocity
#         print "Max flow = ", self.maxFlow
    
    def generateMeshData(self, it, incremental = False):
        coordsStartTime = time.time()
        
        numRows = len(self.heights)
        unitLength = self.length / (numRows-1)
        self.unitArea = unitLength**2
        self.minHeightDifference = unitLength * MIN_POINT_HEIGHT_DIFFERENCE / self.length
        
        if incremental:
            # Delete references to previous Edges/Triangles
            for row in self.meshData["points"]:
                for point in row:
                    point.edges = ()
            for kind,edgeGrid in self.meshData["edges"][it-1].iteritems():
                for edgeRow in edgeGrid:
                    for edge in edgeRow:
                        edge.triangles = ()
            
            # Generate points
            for iR in range(numRows):
                if iR % 2 == 0:
                    halfRow = []
                    for iH in range(1, numRows, 2):
                        bedrockDepth = (self.meshData["points"][iR/2][(iH+1)/2-1].bedrockDepth + self.meshData["points"][iR/2][(iH+1)/2].bedrockDepth)/2.
                        point = ECG.Point(str(self._pointCounter), iR*unitLength, iH*unitLength, self.heights[iR][iH], bedrockDepth)
                        self._pointCounter += 1
                        halfRow.append(point)
                        
                    self.meshData["points"][iR/2] = T.createAlternatingList(self.meshData["points"][iR/2], halfRow)
                    
            halfTable = []
            for iR in range(numRows):
                if iR % 2 != 0:
                    row = []
                    for iH in range(numRows):
                        bedrockDepth = (self.meshData["points"][(iR+1)/2-1][iH].bedrockDepth + self.meshData["points"][(iR+1)/2][iH].bedrockDepth)/2.
                        point = ECG.Point(str(self._pointCounter), iR*unitLength, iH*unitLength, self.heights[iR][iH], bedrockDepth)
                        self._pointCounter += 1
                        row.append(point)
                    halfTable.append(row)
            self.meshData["points"] = T.createAlternatingList(self.meshData["points"], halfTable)
        else:
            points = []
            for iR in range(numRows):
                row = []
                for iH in range(numRows):
#                     point = ECG.Point(str(iR*numRows+iH), iR*unitLength, iH*unitLength, self.heights[iR][iH])
                    point = ECG.Point(str(self._pointCounter), iR*unitLength, iH*unitLength, self.heights[iR][iH])
                    self._pointCounter += 1
                    row.append(point)
                    
#                 row = tuple(row)
                points.append(row)
            self.meshData["points"] = points
        
        # Generate edges
        horEdges = []
        verEdges = []
        diaEdges = []
        for iR in range(numRows):
            horRow = [] # horizontal edges
            verRow = [] # vertical edges
            diaRow = [] # diagonal edges
            for iH in range(numRows):
                if iH < numRows-1:
                    horEdge = ECG.Edge(self.meshData["points"][iR][iH], 
                                       self.meshData["points"][iR][iH+1])
                    
                    horRow.append(horEdge)
                if iR < numRows-1:
                    verEdge = ECG.Edge(self.meshData["points"][iR][iH], 
                                       self.meshData["points"][iR+1][iH])
                    verRow.append(verEdge)
                    if iH < numRows-1:
                        # Form an alternating pattern of diagonal edges
                        Reven = iR % 2 == 0
                        Heven = iH % 2 == 0
                        bothEven = Reven and Heven
                        bothOdd  = not Reven and not Heven
                        sameParity = bothEven or bothOdd
                        j1, j2 = (int(not sameParity), 0), (int(sameParity), 1)
                        diaEdge = ECG.Edge(self.meshData["points"][iR+j1[0]][iH+j1[1]], 
                                           self.meshData["points"][iR+j2[0]][iH+j2[1]])
                        diaEdge.isDiagonal = True
                        diaRow.append(diaEdge)
                
            horRow = tuple(horRow)
            horEdges.append(horRow)
            if iR < numRows-1:
                verRow = tuple(verRow)
                verEdges.append(verRow)
                diaRow = tuple(diaRow)
                diaEdges.append(diaRow)
                
        self.meshData["edges"][it] = {"horizontal": tuple(horEdges), "vertical": tuple(verEdges), "diagonal": tuple(diaEdges)} 
            
        # Generate triangles
        triangles = []
        for iR in range(numRows-1):
            row = []
            for iH in range(numRows-1):
                Reven = iR % 2 == 0
                Heven = iH % 2 == 0
                bothEven = Reven and Heven
                bothOdd  = not Reven and not Heven
                sameParity = bothEven or bothOdd
                
                upperTri = ECG.Triangle( 
                    self.meshData["edges"][it]["horizontal"][iR][iH], 
                    self.meshData["edges"][it]["vertical"][iR][iH + int(sameParity)], 
                    self.meshData["edges"][it]["diagonal"][iR][iH], 
                )
                row.append(upperTri)
                lowerTri = ECG.Triangle( 
                    self.meshData["edges"][it]["horizontal"][iR+1][iH], 
                    self.meshData["edges"][it]["vertical"][iR][iH + int(not sameParity)], 
                    self.meshData["edges"][it]["diagonal"][iR][iH], 
                )
                row.append(lowerTri)
            row = tuple(row)
            triangles.append(row)
        self.meshData["triangles"][it] = tuple(triangles)
        
        # Update points
        for row in self.meshData["points"]:
            for point in row:
                point.updateLowestNeighbor()
        
        print "Calculated coordinates in", time.time() - coordsStartTime, "seconds."
    
    def addRow(self, row1, row2):
        # Add test row1.length == row2.length
        newRow = []
        length = len(row1)
        for i in range(length):
            if self.randomWeights == True:
                noise = random.random()
                meanHeight = noise*row1[i] + (1-noise)*row2[i]
            else:
                meanHeight = (row1[i] + row2[i]) / 2
            height = self.generateHeight(meanHeight)
                
            self.maxHeight = max(height, self.maxHeight)
            self.minHeight = min(height, self.minHeight)
            newRow.append(height)
            
        return newRow
    
#     @T.profile
    
    def defineDrainageBasins(self):
        drainStartTime = time.time()
        # Reset class attributes
        D.basinHeads  = {}
        D.basinsWithLakes = set()
        D.basinBottoms = {}
        D.basinPoints  = {}
        D.basinSediment  = {}
        D.basinMap = {}
        D.externalBasinEntraces = set() 
        
        for row in self.meshData["points"]:
            for point in row:
                point.drainageBasinId = -1
        
        # Explore drainage patterns to define basins
        i = 0
        for row in self.meshData["points"]:
            for point in row:
                if point.drainageBasinId == -1: 
                    D.defineBasin(i, point)
                i += 1
        
        D.findHeads()
        
#         D.manageSubBasins()
        
        # Register values in list for each iteration
        for row in self.meshData["points"]:
            for point in row:
                point.drainageBasinIds.append(point.drainageBasinId)
                if self.currentIteration == self.cumulativeNumIterations:
                    point.drainageBasinIds.append(-1)
            
#         print "Basin heads by id\n"
#         for id in D.basinHeads:
#             print id, D.basinHeads[id]
#         print D.basinsWithLakes
#         for row in self.meshData[self.currentIteration]["points"]:
#             for point in row:
#                 print point.drainageBasinId, point.isInLake(), 
#             print

        print "Defined drainage basins in", time.time() - drainStartTime, "seconds."
    
    def transportSediment(self, it, allowDeposit = True):
        tsStartTime = time.time()
        
        numRows = len(self.meshData["triangles"][it])
        
        # River bed erosion
        extrema = {"min": float("inf"), "max": -float("inf")}
        for row in self.meshData["points"]:
#             continue
            for point in row:
                if point.name == "7":
                    pass
                if point.position.getX() == 25 and point.position.getY() == 25:
                    pass
#                 if point.isLocalMin():
#                     continue
#                 if point.avgFlowVelocity.length() < Ve0:
#                     continue
                if point.isInLake() and not D.isHead(point) :
                    continue
                
                flowValueFactor = MASS_PER_VOLUME_FLUID * point.flow / self.unitArea
#                 flowValueFactor = min(flowValueFactor, 100)
                
                normAvgFlowVelocity = Vec3(point.avgFlowVelocity)
                normAvgFlowVelocity.normalize()
                flowVelocityDirectionFactor = 0.5 * (-normAvgFlowVelocity.dot(point.getNormal()) + 1)
                
                flowVelocityFactor = min(max(1, point.avgFlowVelocity.length()), 10)
#                 flowVelocityFactor = (flowVelocityFactor)**0.5
                flowVelocityFactor = flowVelocityFactor**2
                
                kineticEnergyFactor = flowValueFactor * flowVelocityFactor
                
#                 print flowVelocityDirectionFactor
                
                maxDepth = point.heights[-1] - point.bedrockDepth
                bedrockDepthFactor = -abs(point.heights[-1] - point.bedrockDepth - BEDROCK_DEPTH)/BEDROCK_DEPTH + 1
                bedrockDepthFactor = min(max(bedrockDepthFactor, 0), 1)
                
#                 slopeFactor = point.getSlope().dot(Vec3(0,0,-1))
                
                
                depth = REF * EROSION_DEPTH_FACTOR * kineticEnergyFactor * flowVelocityDirectionFactor
                depth = min(maxDepth, depth)
#                 print depth, flowVelocityDirectionFactor, flowVelocityFactor, flowValueFactor
#                 if point.isInLake():
#                     depth /= point.position.getZ() - D.basinHeadsById[str(point.drainageBasinId)] + 1
#                 depth = - REF * EROSION_DEPTH_FACTOR * flowFactor
                
#                 if len(point.edges) % 4 == 0:
#                 depth = point.getAdmissibleDeltaHeight(depth)
#                 if depth < minValue:
#                     continue
#                 print depth
                extrema["min"] = min(extrema["min"], depth)
                extrema["max"] = max(extrema["max"], depth)
                # Register point position delta after erosion
                point.delta -= depth
                
#             print 
        print "MIN", extrema["min"]
        print "MAX", extrema["max"]
        
        # Rainfall impact erosion
        
        minValue = -REF * EROSION_DEPTH_FACTOR * 15
        endValue = -minValue / 5 
        for iR in range(numRows):
            continue
            row = self.meshData["triangles"][it][iR]
            for tri in row:
                if tri.isInLake():
                    continue
                flowVelocityFactor = tri.avgFlowVelocity.length()
#                 flowVelocityFactor = 0.5 * (point.avgFlowVelocity.length() - Ve0) / Ve0
                flowValueFactor = min(tri.flow / self.unitArea, 100)
                flowVelocityFactor = (flowVelocityFactor)**0.5
                bedrockDepthFactor = -point.position.getZ()/BEDROCK_DEPTH + 1 + point.heights[0]/BEDROCK_DEPTH
                bedrockDepthFactor = min(max(bedrockDepthFactor, 0), 1)
                
                if depth > minValue:
                    continue
                for point in tri.points:
                    # Register point position delta after erosion
                    point.deltaPosition += Vec3.unitZ() * -depth
                
                flow = SedimentTriFlow(-3.0*depth, None, tri) # Because 3 points
                
                # Correct erosion depth
                # Make sure the new height of each point isnt lower than the height of its neighbors
#                     depth = T.limitSedimentDepth(tri, depth, "min")
#                     erosionVec = Vec3.unitZ() * -depth
                
                # Sedimentation
                fc = FC.FlowCalculator(endValue, 1)
                fc.expand(flow)
        
        minValue = -REF * EROSION_DEPTH_FACTOR * 15
        endValue = -minValue / 5 
        for iR in range(numRows):
            continue
            row = self.meshData["triangles"][it][iR]
            for tri in row:
                unitFlux = abs(tri.normal.dot(ug))
                
                # Calculate eroded rock volume
                depth = - REF * EROSION_DEPTH_FACTOR * unitFlux
                
                if depth > minValue:
                    continue
                for point in tri.points:
                    # Register point position delta after erosion
                    point.deltaPosition += Vec3.unitZ() * -depth
                
                flow = SedimentTriFlow(-3.0*depth, None, tri) # Because 3 points
                
                # Correct erosion depth
                # Make sure the new height of each point isnt lower than the height of its neighbors
#                     depth = T.limitSedimentDepth(tri, depth, "min")
#                     erosionVec = Vec3.unitZ() * -depth
                
                # Sedimentation
                fc = FC.FlowCalculator()
                fc.expand(flow)
        
        # Flow speed variation erosion
        extrema = {"min": float("inf"), "max": -float("inf")}
        endValue = FSVEF * EROSION_DEPTH_FACTOR / 100.
        for kind, matrix in self.meshData["edges"][it].iteritems():
#             continue
            length = len(matrix)
            for iR in range(length):
                row = matrix[iR]
                for edge in row:
                    if edge.isInLake():
                        continue
                    if len(edge.triangles) == 1:
                        continue
                    if not edge.isConcave:
                        continue
                    # Consider z==0 as the sea level, no erosion below that
#                     if any(point.position.getZ() < 0.0 for point in edge.points):
#                         continue
                    center = edge.getCenter()
                    degreeOfConcavity = (edge.angle - math.pi) / math.pi
                    edgeNormSpeed = Vec3(edge.avgFlowVelocity)
                    edgeNormSpeed.normalize()
                    
                    for tri in edge.triangles:
                        op = tri.getOppositePoint(edge)
                        vec = center - op.position # vector pointing from the top of the triangle to the center of the edge
                        vec.normalize()
                        
                        dot = vec.dot(edgeNormSpeed)
                        if dot < 0:
                            # Erode only if the flow is directed at the edge
                            continue
                        
                        flowValueFactor = 2 * MASS_PER_VOLUME_FLUID * tri.flow / self.unitArea
                        
                        # Calculate eroded rock volume
                        depth = FSVEF * EROSION_DEPTH_FACTOR * dot * degreeOfConcavity * flowValueFactor
                        
                        erodingPoint = edge.getNeighborTriangle(tri).getOppositePoint(edge)
                        
                        # Correct erosion depth by calculating the max depth
                        erodingPointPos = [erodingPoint.position.getX(), erodingPoint.position.getY(), erodingPoint.position.getZ()] 
                        triNormal = [tri.normal.getX(), tri.normal.getY(), tri.normal.getZ()]
                        maxDepth = erodingPointPos[2] + (triNormal[0]*erodingPointPos[0] + triNormal[1]*erodingPointPos[1]) / triNormal[2] - center.dot(tri.normal) / triNormal[2]
#                         maxDepth = erodingPoint.position.getZ() - center.getZ()
                        if maxDepth < 0:
                            continue
                        depth = min(depth, maxDepth)
                        extrema["min"] = min(extrema["min"], depth)
                        extrema["max"] = max(extrema["max"], depth)

                        erodingPoint.delta -= depth
                     
        print "MIN", extrema["min"]
        print "MAX", extrema["max"]
        
        # Sedimentation
        if allowDeposit:
            for row in self.meshData["points"]:
    #             continue
                for point in row:
                    if point.drainageBasinId != -1 and point.delta < 0:
                        D.addSediment(point.drainageBasinId, -point.delta)
#                     if not point.hasEroded and point.delta < 0:
#                         point.hasEroded = True
#                         abstractSource = ECG.Point("", 0,0,0)
#                         abstractTarget = ECG.Edge(abstractSource, point, False)
#                         abstractTarget.flowDirection = point.name
#                         abstractTarget.avgFlowVelocity = point.avgFlowVelocity
#                         flow = SedimentEdgeFlow(-point.delta, abstractSource, abstractTarget)
#                         fc = FC.FlowCalculator()# numoccurances variation has no effect
#                         fc.expand(flow)
#                 print 
        
            # Deposition on lakes (linear)
            for basinId in D.basinSediment:
                # Total depth of all points in the lake
                lakePoints = filter(lambda p: p.isInLake(), D.basinPoints[basinId])
                H = sum(D.basinHeads[basinId]["height"] - point.position.getZ() for point in lakePoints)
                if H > 0:
                    # Linear deposition coefficient
                    A = D.basinSediment[basinId] / H
                    
                    # Deposit Si = A*Hi
                    for point in lakePoints:
                        Hi = D.basinHeads[basinId]["height"] - point.position.getZ()
                        point.delta += A*Hi
        
        print "Transported sediment in", time.time() - tsStartTime, "seconds."
        print 
    
    def writeColorData(self, cumIt, type):
        # Write the data into the geom row by row
        pointNames = []
        numRows = len(self.meshData["points"])
        deltaIt = self.cumulativeNumIterations - cumIt
        self._dataWriters[cumIt][type]["vertex"].setRow(0)
        self._dataWriters[cumIt][type]["color"].setRow(0)
        for iR in range(numRows):
            for iH in range(numRows):
                point = self.meshData["points"][iR][iH]
                pntItIdx = len(point.heights)
                if deltaIt < pntItIdx:
                    pointNames.append(point.name)
                    height = float(point.heights[pntItIdx - deltaIt - 1]) if self.showSedimentTransport else point.bedrockDepth + BEDROCK_DEPTH
                    position = Vec3(point.position.getX(), point.position.getY(), 0.0)
                    heightVec = Vec3(0.0, 0.0, height)
                    normalizedHeight = 1 if self.heightRange == 0 else (height - self.minHeight) / self.heightRange
                
                    self.writeElevationDatum(cumIt, type, normalizedHeight, position, heightVec)
        
        return pointNames
        
    def writeTextureData(self, cumIt, type):
        pointNames = []
        numRows = len(self.meshData["points"])
        deltaIt = self.cumulativeNumIterations - cumIt
        self._dataWriters[cumIt][type]["vertex"].setRow(0)
        self._dataWriters[cumIt][type]["normal"].setRow(0)
        for iR in range(numRows):
            for iH in range(numRows):
                point = self.meshData["points"][iR][iH]
                pntItIdx = len(point.heights)
                if deltaIt < pntItIdx:
                    pointNames.append(point.name)
                    height = float(point.heights[pntItIdx - deltaIt - 1]) if self.showSedimentTransport else point.bedrockDepth + BEDROCK_DEPTH
                    self._dataWriters[cumIt][type]["vertex"].addData3f( Vec3(point.position.getX(),point.position.getY(),height) )
                    self._dataWriters[cumIt][type]["normal"].addData3f( point.getNormal() )
        return pointNames
    
    def genColorVisu(self, cumIt, type):
        VDStartTime = time.time()
        
        it = self.getGeomIteration(cumIt)
        node = GeomNode("{}-{}".format(cumIt, type))
        model = render.attachNewNode(node)
#         model.setTwoSided(True)

        format = GeomVertexFormat.getV3n3cpt2()
        vdata = GeomVertexData("someVData", format, Geom.UHDynamic)

        if cumIt not in self._dataWriters:
            self._dataWriters[cumIt] = {}
        if type not in self._dataWriters[cumIt]:
            self._dataWriters[cumIt][type] = {}
            self._dataWriters[cumIt][type]["vertex"] = GeomVertexRewriter(vdata, 'vertex')
            self._dataWriters[cumIt][type]["color"] = GeomVertexRewriter(vdata, 'color')
        
        pointNames = self.writeColorData(cumIt, type)
        
        print "Wrote vertex data {} in".format(cumIt), time.time() - VDStartTime, "seconds."
                
        # Fill m-1 Tristrips
        GeomStartTime = time.time()
        numTris = 0
        length = len(self.meshData["triangles"][it])
        for iR in range(length):
            row = self.meshData["triangles"][it][iR]
            
            for tri in row:
                geom = Geom(vdata)
                tris = GeomTriangles(Geom.UHDynamic)
                for point in tri.points:
                    vertexIdx = pointNames.index(point.name)
                    tris.addVertex( vertexIdx )
                tris.closePrimitive()
                geom.addPrimitive(tris)
                node.addGeom(geom)
                numTris += 1
                
        print "Created geometry {} in".format(cumIt), time.time() - GeomStartTime, "seconds."   
        
        self.world.app.clearText()
        self.world.app.genLabelText(type + " visualisation", 0)
        self.world.app.genLabelText("Erosion iterations: " + str(self.cumulativeNumIterations), 1)
        self.world.app.genLabelText("Triangles: " + str(numTris), 2)
        self.world.app.genLabelText("Height range: [" + str(round(self.minHeight,2)) + " : " + str(round(self.maxHeight,2)) + "]", 3)
#         self.world.app.genLabelText("Roughness: " + str(self.roughness), 4)
#         self.world.app.genLabelText("Initial height variability: " + str(self.initHeightVariability), 5)
        
        if cumIt not in self._visualisations:
            self._visualisations[cumIt] = {}
        self._visualisations[cumIt][type] = [model]
        
        self._activeVisu = self._visualisations[cumIt][type]
    
    def genTextureVisu(self, cumIt, type):
        GDMStartTime = time.time()
        
        it = self.getGeomIteration(cumIt)
        
        vformat = GeomVertexFormat.getV3n3t2()
        vdata = GeomVertexData("triVData", vformat, Geom.UHDynamic)
        
        if it not in self._dataWriters:
            self._dataWriters[it] = {}
        self._dataWriters[cumIt][type] = {}
        self._dataWriters[cumIt][type]["vertex"] = GeomVertexRewriter(vdata, 'vertex')
        self._dataWriters[cumIt][type]["normal"] = GeomVertexRewriter(vdata, 'normal')
#         texcoordWriter = GeomVertexRewriter(vdata, 'texcoord')
        
        pointNames = self.writeTextureData(cumIt, type)
        
        if it not in self._visualisations:
            self._visualisations[cumIt] = {}
        self._visualisations[cumIt][type] = []
        
        length = len(self.meshData["triangles"][it])
        for iR in range(length):
            row = self.meshData["triangles"][it][iR]
            
            for tri in row:
                node = GeomNode("someNode")
                model = render.attachNewNode(node)
#                 model.setTwoSided(True)
                geom = Geom(vdata)
                tris = GeomTriangles(Geom.UHDynamic)
                for point in tri.points:
                    vertexIdx = pointNames.index(point.name)
                    tris.addVertex( vertexIdx )
#                     tris.addVertex( int(point.name) )
                tris.closePrimitive()
                geom.addPrimitive(tris)
                node.addGeom(geom)
                
                self._visualisations[cumIt][type].append(model)
                
        self.regenTextureVisu(cumIt, type)
                
        self.world.app.clearText()
        self.world.app.genLabelText(type + " visualisation", 1)
        self.world.app.genLabelText("Iterations: " + str(self.numIterations), 2)
        self.world.app.genLabelText("Flow range: [" + str(round(self.minFlow,2)) + " : " + str(round(self.maxFlow,2)) + "]", 3)
        self.world.app.genLabelText("Speed range: [" + str(round(self.minVelocity,2)) + " : " + str(round(self.maxVelocity,2)) + "]", 4)
        
        self._activeVisu = self._visualisations[cumIt][type]
        
        print "Generated drainage model in", time.time() - GDMStartTime, "seconds."
        
    def regenTextureVisu(self, cumIt, type):
        it = self.getGeomIteration(cumIt)
        eroIt = self.getEroIteration(cumIt)
        
        length = len(self.meshData["triangles"][it])
        iM = 0
        deltaFlow = self.maxFlow - self.minFlow
        deltaVelocity = self.maxVelocity - self.minVelocity
        for iR in range(length):
            row = self.meshData["triangles"][it][iR]
            
            for tri in row:
                model = self._visualisations[cumIt][type][iM]

                colorTexture = Texture()
                colorImage = PNMImage(1, 1)
                
                normalizedValue = 0.0
                if type == "flow":
                    if deltaFlow > 0: 
                        normalizedValue = (tri.flows[eroIt] - self.minFlow) / deltaFlow
                elif type == "velocity":
                    if deltaVelocity > 0: 
                        normalizedValue = (tri.avgFlowVelocities[eroIt] - self.minVelocity) / deltaVelocity
                    
                color = self.getColor(normalizedValue)
                colorImage.fill(color.getX(), color.getY(), color.getZ())
                colorTexture.load(colorImage)
                
                model.setTexture(colorTexture)
                iM += 1
    
    def showVisualisation(self, cumIt, type):
        if self._activeVisu is not None:
            for model in self._activeVisu:
                model.hide()
#         self._activeVisu = type
        it = self.getGeomIteration(cumIt)
        if cumIt in self._visualisations and type in self._visualisations[cumIt]:
#             self.regenVisualisation(cumIt, type)
            self._activeVisu = self._visualisations[cumIt][type]
            for model in self._activeVisu:
                model.show()
        else:
            if type == "elevation":
                self.genColorVisu(cumIt, type)
            else:
                self.genTextureVisu(cumIt, type)
        
    def regenVisualisation(self, cumIt, type):
        if type == "elevation":
            pointNames = self.writeColorData(cumIt, type)
        else:
            self.writeTextureData(cumIt, type)
            self.regenTextureVisu(cumIt, type)
        self.regenDrainageBasins(cumIt)
    
    def genDrainageBasins(self):
        numRows = len(self.heights)
        for iR in range(numRows):
            for iH in range(numRows):
                point = self.meshData["points"][iR][iH]
                text = TextNode(point.name)
                textNodePath = render.attachNewNode(text)
                textNodePath.setScale(30./numRows)
                self.textNodes.append(textNodePath)
    
    def regenDrainageBasins(self, cumIt):
        it = self.getGeomIteration(cumIt)
        deltaIt = self.cumulativeNumIterations - cumIt
        if self.showDrainageBasins:
            if len(self.textNodes) == 0:
                self.genDrainageBasins()
            numRows = len(self.heights)
            iN = 0
            for iR in range(numRows):
                for iH in range(numRows):
                    point = self.meshData["points"][iR][iH]
                    textNodePath = self.textNodes[iN]
                    pntItIdx = len(point.heights)
                    if deltaIt < pntItIdx:
                        shiftedIdx = cumIt - (self.cumulativeNumIterations - pntItIdx + 1)
                        height = float(point.heights[shiftedIdx]) if self.showSedimentTransport else point.heights[0]
                        
                        textNodePath.show()
                        text = ""
                        if shiftedIdx < len(point.drainageBasinIds):
                            if self.labelType == "height":
                                text = str(round(height, 2))
                            elif self.labelType == "basinId":
                                text = str(point.drainageBasinIds[shiftedIdx])
                            elif self.labelType == "flow":
                                text = str(round(point.flows[shiftedIdx], 2))
                            elif self.labelType == "velocity":
                                text = str(round(point.avgFlowVelocities[shiftedIdx], 2))
                            elif self.labelType == "name":
                                text = point.name

                        textNodePath.getNodes()[0].setText(text)
                        textNodePath.setPos(Vec3(point.position.getX(), point.position.getY(), height + 1))
                    else:
                        textNodePath.hide()
                    iN += 1
        else:
            if len(self.textNodes) > 0:
                for textNodePath in self.textNodes:
                    textNodePath.hide()
        
class Planet(Object):
    '''
    classdocs
    '''
        
    class Crater:
        def __init__(self, planet, radius, phi, theta):
            self.planet = planet
            self.radius = radius
            self.phi = phi
            self.theta = theta
            
            numRows = len(planet.heights)
            
            # Calculate the angular width (phi, theta) of the crater
            dPhi = math.asin(radius/planet.radius)
            minPhi = phi - dPhi
            maxPhi = phi + dPhi
            if minPhi <= planet.unitLatitudeAngle: # North pole is in the crater
                minPhi = 0
            elif maxPhi > (numRows-2)*planet.unitLatitudeAngle: # South pole is in the crater
                maxPhi = math.pi
            else: # Both poles are outside the crater
                dTheta = math.asin( radius/(planet.radius*math.sin(phi)) )
                minTheta = (theta - dTheta) % (2*math.pi)
                maxTheta = (theta + dTheta) % (2*math.pi)

            # Calculate the basis change quaternion
            yAxis = Vec3(0,1,0)
            qTheta = Quat(math.cos(theta/2), 0, 0, math.sin(theta/2))
            qTheta.normalize()
            newYAxis = qTheta.xform(yAxis)
            newYAxis.normalize()
            newYAxis *= math.sin( phi / 2)
            qPhi = Quat(math.cos( phi / 2 ), newYAxis)
            qPhi.normalize()
            qTot = qTheta*qPhi
            qTot.normalize()
            self.qTotInv = Quat(qTot)
            self.qTotInv.invertInPlace()
             
            # Iterate over all vertices         
            self.craterVertices = {}
            self.minRho = radius
            self.vertexClosestToImpact = {}
            self.impactHeight = 0

            for iR in range(numRows):
                row = planet.sphereVertexData[iR]
#                 for iH in range(row['length']):
#                             vertex = row['vertices'][iH]
#                             addCraterHeight(vertex, row['phi'])
#                 continue
                if minPhi <= row['phi'] and row['phi'] <= maxPhi:
                    try:
                        dTheta
                    except NameError: # No conditions on theta
                        for iH in range(row['length']):
                            vertex = row['vertices'][iH]
                            self.checkVertex(vertex, row['phi'])
                    else:
                        for iH in range(row['length']):
                            vertex = row['vertices'][iH]
                            if minTheta < maxTheta:
                                if minTheta <= vertex['theta'] and vertex['theta'] <= maxTheta:
                                    self.checkVertex(vertex, row['phi'])
                                elif maxTheta < vertex['theta']:
                                    break
                            else:
                                if minTheta <= vertex['theta'] or vertex['theta'] <= maxTheta:
                                    self.checkVertex(vertex, row['phi'])
                                    
                elif maxPhi < row['phi']:
                    break
            self.impactHeight = self.vertexClosestToImpact["height"]
            
            # Add crater heights
            for rho,vertexList in self.craterVertices.iteritems():
                for vertex in vertexList:
                    self.addHeight(vertex, float(rho)) 
            
        def checkVertex(self, vertex, rowPhi):
            # Calculate the vertex's position in the new basis
            position = Vec3(vertex['x'], vertex['y'], vertex['z'])
            newPosition = self.qTotInv.xform(position)
            x,y,z = newPosition.getX(), newPosition.getY(), newPosition.getZ()
            x,y,z = round(x,6), round(y,6), round(z,6)
            if z <= 0:
                return
            elif self.planet.radius > abs(z):
                # The distance from the circle's center is given by:
                rho = math.sqrt( x**2 + y**2 )
            else:
                return
            # Check if vertices are inside the circle
            if rho <= self.radius:
                if str(rho) not in self.craterVertices:
                    self.craterVertices[str(rho)] = []
                self.craterVertices[str(rho)].append(vertex)
                
                if rho < self.minRho:
                    self.minRho = rho
                    self.vertexClosestToImpact = vertex
                                  
        def addHeight(self, vertex, rho):
            proximity = rho/self.radius
            coef = 0.05 # def 0.1
            heightInCrater = -coef*rho**2/self.radius + coef*self.radius

            if proximity > 0.9:
                # Blend between the outside terrain and the rim's edge
                b = (proximity-0.9)/0.1
#                 vertex['height'] = b*vertex['height'] + (1-b)*(self.impactHeight + heightInCrater)
                vertex['height'] = self.impactHeight + heightInCrater
            else:
                # Blend in some procedural terrain to give craters some detail
                b = 0.5
#                 vertex['height'] = b*vertex['height'] + (1-b)*(self.impactHeight - heightInCrater)
                vertex['height'] = self.impactHeight - heightInCrater
     
            self.planet.maxHeight = max(vertex['height'], self.planet.maxHeight)
            self.planet.minHeight = min(vertex['height'], self.planet.minHeight)
                    
    def __init__(self, world, radius, numIterations, law, heightVariability, roughness, colors, randomWeights, numCraters):
        '''
        Constructor
        '''
        Object.__init__(self, world, numIterations, law, heightVariability, roughness, colors, randomWeights)
        # A minimal heights matrix is 2x3 + poles
        pole = T.CircularList([0])
        line = T.CircularList([0,0,0])
        self.heights = [pole,
                        line,
                        line,
                        pole]

        self.radius = float(radius)
        self.numCraters = int(numCraters)
        
        heightsStartTime = time.time()
        for it in range(self.numIterations):
            self.generateHeights()
            self.densify()
        print "Calculated heights in", time.time() - heightsStartTime, "seconds."
        
        self.generateMeshData()
        
        if numCraters > 0:
            self.addCraters()
            
        self.heightRange = self.maxHeight - self.minHeight
        
        self.genColorVisu()
        
        print "Finished in", time.time() - heightsStartTime, "seconds."

    def generateMeshData(self):
        coordsStartTime = time.time()
        self.sphereVertexData = []
        numRows = len(self.heights)
        self.unitLatitudeAngle = math.pi / (numRows - 1)
        
        for iR in range(numRows):
            row = {'phi': iR*self.unitLatitudeAngle, 'vertices': T.CircularList(), 'length': len(self.heights[iR])}
            
            # Create vertices along a circle
            radius = self.radius * math.sin( iR * self.unitLatitudeAngle )
            height = self.radius * math.cos( iR * self.unitLatitudeAngle )
            cRadius = complex(radius,0)
                
            unitLongitudeAngle = 2*math.pi / row['length']
            cUnitLongitudeAngle = cmath.exp(unitLongitudeAngle*1j)
            
            for iH in range(row['length']):
                vertex = {'theta': iH*unitLongitudeAngle, 'x': cRadius.real, 'y': cRadius.imag, 'z': height, 'height': self.heights[iR][iH]}
                row['vertices'].append(vertex)
                cRadius *= cUnitLongitudeAngle
                
            self.sphereVertexData.append(row)
            
        self.sphereVertexData = tuple(self.sphereVertexData)
        
        print "Calculated coordinates in", time.time() - coordsStartTime, "seconds."

    def addRow(self, row1, row2):
        newRow = T.CircularList()
        row1Length = len(row1)
        row2Length = len(row2)
        
        if row1Length == 1 or row2Length == 1:
            if row1Length == 1: # North pole :)
                poleHeight = row1[0]
                row = row2
            elif row2Length == 1: # South pole
                poleHeight = row2[0]
                row = row1
            length = 3 # vertices on the new polar circle
            
        else:
            length = min( row1Length, row2Length )
            
        for i in range(length):
            # Calculate mean height
            if self.randomWeights == True:
                noise = random.random()
                factor1, factor2 = noise, 1-noise
            else:
                factor1, factor2 = 0.5, 0.5
            try:
                poleHeight
            except NameError:
                if row1Length < row2Length:
                    meanHeight = factor1*row1[i] + factor2*row2[2*i]
                elif row1Length > row2Length:
                    meanHeight = factor1*row2[i] + factor2*row1[2*i]
                else:
                    meanHeight = factor1*row1[i] + factor2*row2[i]
            else:
                meanHeight = factor1*poleHeight + factor2*row[2*i]
                
            height = self.generateHeight(meanHeight)
            newRow.append(height)
            
        return newRow
    
    def densify(self):
        # This function makes the amount of vertices per row proportional to the circle's length
        
        # Calculate longitudinal vertex density
        length = len(self.heights)
        longVDensity = (2*length-2) / (2*math.pi*self.radius)
        # Adapt latitudinal vertex density to match the longitudinal one, which is always higher
        self.unitLatitudeAngle = math.pi / (length - 1)
        for iR in range(1, length-1):
            latVDensity = len(self.heights[iR]) / (2*math.pi*self.radius*math.sin(iR * self.unitLatitudeAngle))
            densityRatio = longVDensity / latVDensity
            numIters = int(round( math.log(densityRatio, 2), 0))
            for m in range(numIters):
                self.heights[iR] = self.interpolateHeights(self.heights[iR])
            
    def addCraters(self):
        cratersStartTime = time.time()
                
        # Generate craters
        for c in range(self.numCraters):
            u,v = random.random(), random.random()
            radius = min( (random.gammavariate(0.1, 0.15) + 0.05)*self.radius, 0.3*self.radius)
            phi = math.acos(2*u-1)
            theta = 2*math.pi*v
            Planet.Crater(self, radius, phi, theta)
#         
        print "Generated craters in", time.time() - cratersStartTime, "seconds."
        
    def writeVertexData(self):
        VDStartTime = time.time()
        numRows = len(self.heights)
        vformat = GeomVertexFormat.getV3c4()
        vdata = GeomVertexData("someVData", vformat, Geom.UHDynamic)

        self._dataWriters["elevation"]["vertex"] = GeomVertexRewriter(vdata, 'vertex')
        self._dataWriters["elevation"]["color"] = GeomVertexRewriter(vdata, 'color')
        
        self.VDindexes = []
        currentVDindex = 0
        
        for iR in range(numRows):
            row = self.sphereVertexData[iR]['vertices']
            rowLength = self.sphereVertexData[iR]['length']
            self.VDindexes.append(T.CircularList())
            
            # If it's a pole
            if iR % (numRows - 1) == 0:
                if iR == 0: 
                    self._dataWriters["elevation"]["vertex"].addData3f( 0, 0, self.radius + row[iR]['height'])
                    self._dataWriters["elevation"]["color"].addData3f( 0, 0, 1)
#                     self._dataWriters["elevation"]["color"].addData3f( 1, 1, 1)
                elif iR == (numRows - 1): 
                    self._dataWriters["elevation"]["vertex"].addData3f( 0, 0,-(self.radius + row[iR]['height']))
                    self._dataWriters["elevation"]["color"].addData3f( 1, 0, 0)
#                     self._dataWriters["elevation"]["color"].addData3f( 1, 1, 1)
                self.VDindexes[-1].append(currentVDindex)
                currentVDindex += 1
                normalizedHeight = (row[0]['height'] - self.minHeight) / self.heightRange                
                continue
                        
            for iH in range(rowLength):
                vertex = row[iH]
                position = Vec3(vertex['x'], vertex['y'], vertex['z'])
                heightVec = Vec3(position)
                heightVec.normalize()
                heightVec *= vertex['height']
                normalizedHeight = (vertex['height'] - self.minHeight) / self.heightRange       
                 
                self.writeElevationDatum(normalizedHeight, position, heightVec)
#                 self.writeElevationDatum(0, position, Vec3(0,0,0))
#                 self._dataWriters["elevation"]["vertex"].addData3f( position )
#                 if iR % 2 == 0:
#                     self._dataWriters["elevation"]["color"].addData3f( 1, 1, 1)
#                 else:
#                     self._dataWriters["elevation"]["color"].addData3f( 1, 1, 1)
                    
                self.VDindexes[-1].append(currentVDindex)
                currentVDindex += 1
#                 self.texcoordWriter.addData2f((vec[0]+x)/2.0+0.5,(vec[1]+y)/2.0+0.5)
            
        print "Wrote vertex data in", time.time() - VDStartTime, "seconds."
        return vdata
 
    def createGeom(self, vdata):
        GeomStartTime = time.time()
        def addTrifans(length, row1VDidxs, row2VDidxs):
            for iH in range(length):
                trifans = GeomTrifans(Geom.UHDynamic)
                trifans.addVertex(row2VDidxs[2*iH + 1])
                trifans.addVertex(row2VDidxs[2*iH])
                trifans.addVertex(row1VDidxs[iH])
                trifans.addVertex(row1VDidxs[iH + 1])
                trifans.addVertex(row2VDidxs[2*iH + 2])
                trifans.closePrimitive()
                geom.addPrimitive(trifans)
                self.numTris += 3 * length   
                
        def addPolarTrifans(rowLength, poleIdx, rowVDidx):
            trifans = GeomTrifans(Geom.UHDynamic)
            trifans.addVertex(poleIdx)
            trifans.addConsecutiveVertices(rowVDidx, rowLength)
            trifans.addVertex(rowVDidx)
            trifans.closePrimitive()
            geom.addPrimitive(trifans)
            self.numTris += rowLength 
            
        self.numTris = 0
        numRows = len(self.heights)
        geom = Geom(vdata)
                
                
#         axis = GeomLinestrips(Geom.UHDynamic)
#         axis.addVertex(0)
#         axis.addVertex(self.VDindexes[numRows-1][0])
#         axis.closePrimitive()
#         geom.addPrimitive(axis)
# 
#         print self.VDindexes
#         for iR in range(1,numRows):
#             print self.VDindexes[iR][0], len(self.VDindexes[iR])
#             circle = GeomLinestrips(Geom.UHDynamic)
#             for iH in range(len(self.VDindexes[iR])):
#                 circle.addVertex(self.VDindexes[iR][iH]) 
# #             circle.addConsecutiveVertices(self.VDindexes[iR][0], 3)
#             circle.addVertex(self.VDindexes[iR][0])
#             circle.closePrimitive()
#             geom.addPrimitive(circle)
#              
#         return geom
                
        for iR in range(numRows-1):
            row1VDIndexes = self.VDindexes[iR]
            row2VDIndexes = self.VDindexes[iR+1]
            row1Length = len(self.heights[iR])
            row2Length = len(self.heights[iR+1])
            
            
            # If there's a pole involved, create a cone
            if row1Length == 1:
                addPolarTrifans(row2Length, row1VDIndexes[0], row2VDIndexes[0])
            elif row2Length == 1:
                addPolarTrifans(row1Length, row2VDIndexes[0], row1VDIndexes[0])                
            else:                
                if row1Length == row2Length:
                    # Sew the vertices together with tristrips
                    tristrips = GeomTristrips(Geom.UHDynamic)
                    for iH in range(row1Length+1):
                        tristrips.addVertex(row1VDIndexes[iH])
                        tristrips.addVertex(row2VDIndexes[iH])
 
                    tristrips.closePrimitive()
                    geom.addPrimitive(tristrips)
                    self.numTris += 2 * row1Length
                else:
                    # If the number of vertices is different, use trifans
                    if row1Length < row2Length:
                        addTrifans(row1Length, row1VDIndexes, row2VDIndexes)
                    elif row1Length > row2Length:
                        addTrifans(row2Length, row2VDIndexes, row1VDIndexes)  
                        
        print "Created geometry in", time.time() - GeomStartTime, "seconds."   
        return geom
                      
    def genColorVisu(self):
        self.node = GeomNode("someNode")
        self.model = render.attachNewNode(self.node)
        self.model.setTwoSided(True)
#         self.model.setRenderModeWireframe(True)
        
        # Write the data into the geom row by row
        vdata = self.writeVertexData()
        
        # Create the Geom                
        geom = self.createGeom(vdata)
        
        self.node.addGeom(geom)
        
        self.world.app.genLabelText("Iterations: " + str(self.numIterations), 1)
        self.world.app.genLabelText("Triangles: " + str(self.numTris), 2)
        self.world.app.genLabelText("Height range: [" + str(round(self.minHeight,2)) + " : " + str(round(self.maxHeight,2)) + "]", 3)
        self.world.app.genLabelText("Roughness: " + str(self.roughness), 4)
        self.world.app.genLabelText("Initial height variability: " + str(self.initHeightVariability), 5)
        self.world.app.genLabelText("Craters: " + str(self.numCraters), 6)
        

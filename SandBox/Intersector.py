'''
Created on 8 december 2017

@author: Rafael
'''

import math
import Tools as T

class Intersector(object):
    '''
    classdocs
    '''


    def __init__(self, tri, domain, sourceEdge):
        '''
        Constructor
        '''
        i = domain["index"] % 3 # direction irrelevant
        iEdge1 = T.perm(i, [1,2])
        iEdge3 = T.perm(i, [0,2])
        iPoint = T.perm(i, [0,1])
        self.edge1 = tri.edges[iEdge1]
        self.edge3 = tri.edges[iEdge3]
        self.edge2 = tri.edges[iPoint]
        point2 = tri.points[iPoint]
        
        # Calculate length of intersecting segment
        angle2 = domain["angle"]
        angle3 = tri.angles[point2.name]
        angle1 = math.pi - angle2 - angle3
        self.maxIntLength = self.edge1.length *  math.sin(angle3) / math.sin(angle1)
        self.lengthProp2  = self.edge1.length * (math.sin(angle2) / math.sin(angle1)) / self.edge2.length
        self.sourceEdge = sourceEdge
        
        self.intersections = {}
        
    def calculateIntersections(self, *args):
        for sourcePosition in args:
            intersection = {}
            
            if   self.edge1 is self.sourceEdge or self.edge3 is self.sourceEdge:
                if   self.edge1 is self.sourceEdge:
                    simFactor = 1 - sourcePosition
                    lengthProportion = self.lengthProp2 * simFactor
                
                elif self.edge3 is self.sourceEdge:
                    simFactor = sourcePosition
                    lengthProportion = 1 - (1 - self.lengthProp2) * simFactor
                
                edgeName = self.edge2.name
                intersection["edgeIdx"] = 2
                
            elif self.edge2 is self.sourceEdge:
                # Calculate the intersection point for the case where length is maximal
                # to compare it with sourcePosition and see which edge is intersected
                if   sourcePosition < self.lengthProp2:
                    # Edge1 gets intersected
                    simFactor = sourcePosition / self.lengthProp2
                    lengthProportion = 1 - simFactor
                    edgeName = self.edge1.name
                    intersection["edgeIdx"] = 1
                
                elif sourcePosition > self.lengthProp2:
                    # Edge3 gets intersected
                    simFactor = (1 - sourcePosition) / (1 - self.lengthProp2)
                    lengthProportion = simFactor
                    edgeName = self.edge3.name
                    intersection["edgeIdx"] = 3
                
                else:
                    # Either edge gets intersected
                    simFactor = 1
                    lengthProportion = None
                    edgeName = "any"
                    intersection["edgeIdx"] = None
                
            else:
                raise Exception("Wrong source edge for intersection")
            
            length = max(0, self.maxIntLength * simFactor)
            lengthProportion = min(1, max(0, lengthProportion))
            
            intersection["length"] = length
            intersection["outPosition"] = lengthProportion # intersection's position on the edge belongs to ]0,1[ with respect to edge's orientation in the triangle
            intersection["inPosition"] = sourcePosition
            
            self._addIntersection(edgeName, intersection)
        
        # Manage ambivalent edge
        if "any" in self.intersections:
            int1 = dict(self.intersections["any"][0])
            int1["outPosition"] = 0
            int1["edgeIdx"] = 1
            self._addIntersection(self.edge1.name, int1)
            
            int3 = dict(self.intersections["any"][0])
            int3["outPosition"] = 1
            int3["edgeIdx"] = 3
            self._addIntersection(self.edge3.name, int3)
            
            del self.intersections["any"]
        
    def _addIntersection(self, edgeName, intersection):
        if not edgeName in self.intersections:
            self.intersections[edgeName] = []
        self.intersections[edgeName].append(intersection)


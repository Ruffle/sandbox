'''
Created on 18/09/2016

@author: Rafal Starzyk
'''

from panda3d.core import Vec3, LVector3f

import ErosionComputationGeometries as ECG
import Flow as F
import unittest

import math
from Globals import GRAVITY

approximation = 3

# Define tests
testConfigs = (
    [[30, 15, 30],
     [15,  0, 15],
     [30, 15, 30]],
)
testFlowPairs = (
    # 0 - this test may raise a "phantom" error if all other tests pass, 
    #     it doesn't do it if another test fails
    {"in": (10.0, Vec3(1,1,0), None, []), 
     "out": ((10.0, Vec3(1,1,0), "2-3", []),)},
    # 1 
    {"in": (10.0, Vec3(-1,-1,0), "2-3", []), 
     "out": ((5.0, Vec3(-1,-1,0), "3-1", []),
             (5.0, Vec3(-1,-1,0), "1-2", []))},
    # 2
    {"in": (10.0, Vec3(-1,-1,0), "2-3", [Vec3(0.75,0.25,0), Vec3(0.25,0.75,0)]), 
     "out": ((5.0, Vec3(-1,-1,0), "3-1", [Vec3(0,0.5,0), Vec3(0,0,0)]),
             (5.0, Vec3(-1,-1,0), "1-2", [Vec3(0.5,0,0), Vec3(0,0,0)]))},
    # 3            
    {"in": (10.0, Vec3(1,0,0), "3-1", [Vec3(0,0,0), Vec3(0,0.5,0)]), 
     "out": ((10.0, Vec3(1,0,0), "2-3", [Vec3(0.5,0.5,0), Vec3(1,0,0)]),)},
    # 4           
    {"in": (10.0, Vec3(1,0,0), "3-1", []), 
     "out": ((10.0, Vec3(1,0,0), "2-3", [Vec3(0,1,0), Vec3(1,0,0)]),)},
    # 5           
    {"in": (10.0, Vec3(0.75,-0.25,0), "3-1", [Vec3(0,0,0), Vec3(0,0.5,0)]), 
     "out": ((3.333, Vec3(0.75,-0.25,0), "1-2", [Vec3(0,0,0), Vec3(1,0,0)]),
             (6.667, Vec3(0.75,-0.25,0), "2-3", [Vec3(0.75,0.25,0), Vec3(1,0,0)]))},
    # 6           
    {"in": (10.0, Vec3(1,1,0), "1-2", []), 
     "out": ((10.0, Vec3(1,1,0), "2-3", [Vec3(0.5,0.5,0), Vec3(1,0,0)]),)},
    # 7           
    {"in": (10.0, Vec3(0,1,0), "1-2", [Vec3(1,0,0), Vec3(0.5,0,0)]), 
     "out": ((10.0, Vec3(0,1,0), "2-3", [Vec3(0.5,0.5,0), Vec3(1,0,0)]),)},
    # 8
    {"in": (10.0, Vec3(0,1,0), "1-2", [Vec3(0.8,0,0), Vec3(0.4,0,0)]), 
     "out": ((10.0, Vec3(0,1,0), "2-3", [Vec3(0.4,0.6,0), Vec3(0.8,0.2,0)]),)},
    # 9
    {"in": (10.0, Vec3(0,0,0), None, []), 
     "out": ((3.333, Vec3(0,0,0), "2-3", []), 
             (3.333, Vec3(0,0,0), "3-1", []), 
             (3.333, Vec3(0,0,0), "1-2", []))},
)

BASE_SPEED = math.sqrt(2 * GRAVITY) # theta == 0

# Test two bounces
psi = 5.0*math.pi/12.0
a = 1.0 / ( math.sqrt(1+math.tan(psi)**2) * (1+2*math.cos(psi)) + 1 )
min = a
max = 3*a
avgLen = 1.0 / ( (1+2*math.cos(psi)) + 1 )

avgLengths = (1.0, math.sqrt(3)/2.0, math.sqrt(3)/2.0, avgLen)

testFlowPairsCorrection = (
    # 0 test_one_bounce
    {"in":   (10.0, 
              Vec3( BASE_SPEED * avgLengths[0] / (2.0 * math.sqrt(3)), 0, BASE_SPEED * avgLengths[0] / 2.0), 
              "3-1", 
              []), 
     
     "out": ((10.0, 
              Vec3( BASE_SPEED * avgLengths[0] / (2.0 * math.sqrt(3)), 0, -3.0 * BASE_SPEED * avgLengths[0] / 2.0), 
              "1-2", 
              []),)},
    # 1 test_summing
    {"in":   (10.0, 
              Vec3( BASE_SPEED * math.sqrt(3) * avgLengths[1] / 2.0, 0, BASE_SPEED * avgLengths[1] / 2.0), 
              "3-1", 
              []), 
     
     "out": ((10.0, 
              Vec3( BASE_SPEED * math.sqrt(3) * avgLengths[1] / 2.0, 0, -2 * BASE_SPEED * avgLengths[1] / 2.0), 
              "1-2", 
              []),)},
    # 2 test_bounceback
    {"in":   (5.0, 
              Vec3( 0, 0, BASE_SPEED * avgLengths[2] / 2.0), 
              "3-1", 
              []), 
     
     "out": ()},
    # 3 test_two_bounces
    {"in":   (10.0, 
              Vec3( BASE_SPEED * avgLengths[3] / math.tan(psi), 0, BASE_SPEED * avgLengths[3]), 
              "1-2", 
              [Vec3(min, 0.0, 0.0), Vec3(max, 0.0, 0)]), 
     
     "out": ()},
)

class BaseTestOutflows(unittest.TestCase):
    def parseFlow(self, flowTuple, inflow = True):
        sourceEdge = self.tri.getEdgeByName(flowTuple[2])
        sourceEdgePositions = flowTuple[3] if len(flowTuple[3]) == 0 and sourceEdge is None else [point.position for point in sourceEdge.points]
        return F.FluidTriFlow(round(flowTuple[0],approximation), sourceEdge, self.tri if inflow else None, flowTuple[1], sourceEdgePositions, 1.)
    
    def compareVecs(self, vec1, vec2, numDecimals): # Because the built in method doesnt work
        comp1 = [vec1.getX(), vec1.getY(), vec1.getZ()]
        comp2 = [vec2.getX(), vec2.getY(), vec2.getZ()]
        for i in range(3):
            if round(comp1[i], numDecimals) != round(comp2[i], numDecimals):
                return False
        return True
        
    def compareItems(self, item1, item2):
        if isinstance(item1, LVector3f):
            self.assertTrue( self.compareVecs(item1, item2, approximation) )
        elif hasattr(item1, '__iter__'):
            for i in range(len(item1)):
                self.compareItems(item1[i], item2[i])
        else:
            self.assertEqual(item1, item2)

class TestOutflows(BaseTestOutflows):
    def setUp(self):
        pt1 = ECG.Point("1", 0, 0, 0)
        pt2 = ECG.Point("2", 1, 0, 0)
        pt3 = ECG.Point("3", 0, 1, 0)
        
        eg1 = ECG.Edge(pt1, pt2)
        eg2 = ECG.Edge(pt2, pt3)
        eg3 = ECG.Edge(pt3, pt1)
    
        self.tri = ECG.Triangle(eg1, eg2, eg3)

class TestOutflowsCorrection1(BaseTestOutflows):
    def setUp(self):
        pt1 = ECG.Point("1",  0, 0, 0)
        pt2 = ECG.Point("2",  1, 0, math.sqrt(3))
        pt3 = ECG.Point("3", -1, 0, math.sqrt(3))
        
        eg1 = ECG.Edge(pt1, pt2)
        eg2 = ECG.Edge(pt2, pt3)
        eg3 = ECG.Edge(pt3, pt1)
    
        self.tri = ECG.Triangle(eg1, eg2, eg3)

class TestOutflowsCorrection2(BaseTestOutflows):
    def setUp(self):
        pt1 = ECG.Point("1", 0, 0, 0)
        pt2 = ECG.Point("2", 2, 0, 0)
        pt3 = ECG.Point("3", 1, 0, math.sqrt(3))
        
        eg1 = ECG.Edge(pt1, pt2)
        eg2 = ECG.Edge(pt2, pt3)
        eg3 = ECG.Edge(pt3, pt1)
        
        self.tri = ECG.Triangle(eg1, eg2, eg3)

def makeOutflowCalculationTest(testFlowPair):
    def testFunction(self):
        inputFlow   = self.parseFlow(testFlowPair["in"])
        outputFlows = map(lambda flow: self.parseFlow(flow, False), testFlowPair["out"])
        calcResults = inputFlow.calculateOutflows(True)
        
#         print outputFlows, ""
#         print calcResults, ""
        
        self.assertEqual(len(calcResults), len(outputFlows))
        
        sortedResults = sorted(calcResults, key=lambda flow: flow.source.name)
        sortedFlows   = sorted(outputFlows, key=lambda flow: flow.source.name)
        
        for i in range(len(calcResults)):
            self.compareItems(sortedResults[i], sortedFlows[i])
            
    return testFunction
      
for i in range(len(testFlowPairs)):# Add outflow tests
    pass
#     setattr(TestOutflows, "test_" + str(i), makeOutflowCalculationTest(testFlowPairs[i]))

setattr(TestOutflowsCorrection1, "test_one_bounce",  makeOutflowCalculationTest(testFlowPairsCorrection[0]))
# setattr(TestOutflowsCorrection1, "test_summing",     makeOutflowCalculationTest(testFlowPairsCorrection[1]))
# setattr(TestOutflowsCorrection1, "test_bounceback",  makeOutflowCalculationTest(testFlowPairsCorrection[2]))
# setattr(TestOutflowsCorrection2, "test_two_bounces", makeOutflowCalculationTest(testFlowPairsCorrection[3]))

# Run tests
if __name__ == '__main__':
    unittest.main()
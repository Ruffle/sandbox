'''
Created on 24 sept. 2016

@author: Rafael
'''
import math
from panda3d.core import Vec3, TextNode

import Drainage as D
import Flow as F
from Globals import *
import Tools as T


# Erosion computation elements
class Triangle:
    '''
    Triangle with edges directed so that the cross product of two consecutive edges will yield an upward vector.
    '''
    
    def __init__(self, edge1, edge2, edge3):
        self.edges  = set([edge1, edge2, edge3])
        if len(self.edges) < 3:
            raise Exception("Duplicate edges in Triangle creation.")
        self.directedEdges = []
        self.edges = list(self.edges)
        
        self.points = set(edge1.points + edge2.points + edge3.points)
        if len(self.points) > 3:
            raise Exception("Disjoined edges in Triangle creation.")
        
        self.points = list(self.points)
        self.points.sort(key=lambda pnt: Vec3.length(pnt.position))
        
        # Let the first point of the set be the "main" point of the triangle
        # Figure out which orientation of the triangle would yield an upwards normal vector
        self.orientate(0,1,2) 
        self.orientate(0,2,1)
        
        self._updateVectors()
        
        for edge in self.edges:
            edge.addTriangle(self)
            if len(edge.triangles) == 2:
                edge.calculateAngle()
        
        self.name = self.points[0].name + '-' + self.points[1].name + '-' + self.points[2].name
        self.flow = 0.0 # Total quantity of flows passing through the triangle
#         self.flowVelocity = Vec3(0,0,0)
        self.flowVelocityNorm = 0
        self.flowVelocityDirection = Vec3(0,0,0)
        self.approxFlowDirection = Vec3(0,0,0)
        self.approxFlowVelocity = Vec3(0,0,0)
        self.avgFlowVelocity = Vec3(0,0,0)
        self.flows = [] #flows per iteration
        self.avgFlowVelocities = [] #velocities per iteration
        self.flowNames = [] # names of flows which have passed through this triangle
        self.order = TextNode(self.name)
        textNodePath = render.attachNewNode(self.order)
        textNodePath.setPos(self.getCenter() + Vec3(0,0,1))
        
        self._computeAngularDomains()
         
    def _updateVectors(self):
        # Updates the normal, the slope and the direction of edges
        vec1 = self.points[1].position - self.points[0].position
        vec2 = self.points[2].position - self.points[1].position
        vec3 = self.points[0].position - self.points[2].position
        res  = vec1.cross(vec2)
        
        self.normal = res
        vec1.normalize()
        vec2.normalize()
        vec3.normalize()
        
        self.directedEdges = (
            {"edge": self.edges[0], "vector": vec1, "orientation": self.points[0].name + self.points[1].name},
            {"edge": self.edges[1], "vector": vec2, "orientation": self.points[1].name + self.points[2].name},
            {"edge": self.edges[2], "vector": vec3, "orientation": self.points[2].name + self.points[0].name},
        )
        self.area = Vec3.length(self.normal)/2
        self.normal.normalize()  
        self.slope = (self.normal.cross(ug)).cross(self.normal)
        self.slope.normalize()
        self.cosTheta = self.slope.dot(ug)
        
    def _computeAngularDomains(self):
        # Calculate the 6 possible angular domains for the flow
        vec1 = self.directedEdges[0]["vector"]
        vec2 = self.directedEdges[1]["vector"]
        vec3 = self.directedEdges[2]["vector"]
        
        angle12 = T.calculateAngle(vec1, vec2, self.normal)
        angle13 = T.calculateAngle(vec1, vec3, self.normal)
        
        self.angularDomains = [
            angle13-math.pi, 
            angle12, 
            math.pi, 
            angle13, 
            angle12+math.pi, 
            2*math.pi, 
        ]
        self.domainToEdges = [(), (), (), (), (), ()]
        
        # Define edges corresponding to each domain
        indices = T.CircularList([0,1,2])
        for i in range(3):
            self.addEdgeToDomain(indices[i+1], 2*i)
            self.addEdgeToDomain(indices[i+1], 2*i+1)
            self.addEdgeToDomain(indices[i+2], 2*i+1)

        # Set triangle's internal angles
        angle0 = round(angle13-math.pi, approximation)
        angle1 = round(math.pi-angle12, approximation)
        angle2 = math.pi - angle0 - angle1
        self.angles = {
            self.points[0].name: angle0,
            self.points[1].name: angle1,
            self.points[2].name: angle2,
        }
        
    def __repr__(self):
        return ("Triangle('" + self.name
                            + "', area=" + str(self.area)
                            + ", normal=" + str(self.normal)
                            + ", slope=" + str(self.slope)
                + ")")
    
    def orientate(self, i1, i2, i3):
        # Attempts to orient the triangle
        vec1 = self.points[i2].position - self.points[i1].position
        vec2 = self.points[i3].position - self.points[i2].position
        vec3 = self.points[i1].position - self.points[i3].position
        res  = vec1.cross(vec2)
        
        if res.dot(ug) <= 0:
            # Order points/edges 
            self.edges = T.CircularList([
                self.getEdgeFromPoints(self.points[i2], self.points[i1]), 
                self.getEdgeFromPoints(self.points[i3], self.points[i2]),
                self.getEdgeFromPoints(self.points[i1], self.points[i3]),
            ])
            self.points = T.CircularList([
                self.points[i1],
                self.points[i2],
                self.points[i3],
            ])
            
            return True

        return False
    
    def getPointByPosition(self, position):
        for point in self.points:
            if point.position == position:
                return point
        return None
    
    def getEdgeByName(self, name):
#         reversedName = name[::-1]
        for edge in self.edges:
#             if edge.name == name or edge.name == reversedName:
            if edge.name == name:
                return edge
        return None
    
    def getEdgeFromPoints(self, point1, point2):
        name1 = point1.name + '-' + point2.name
        name2 = point2.name + '-' + point1.name
        for edge in self.edges:
            if edge.name == name1 or edge.name == name2:
                return edge
        return None
    
    def getEdgeOrientation(self, edgeName):
        assert isinstance(edgeName, basestring), "edgeName must be a string"
        if self.directedEdges != None:
            for directedEdge in self.directedEdges:
                if directedEdge["edge"].name == edgeName:
                    return directedEdge["orientation"]
        
        raise Exception("Cannot retrieve edge orientation because the triangle doesn't have an orientation.")
    
    def addEdgeToDomain(self, iE, iD):
        dte = list(self.domainToEdges[iD])
        dte.append(self.directedEdges[iE]["edge"])
        self.domainToEdges[iD] = tuple(dte)
    
    def getOppositePoint(self, edge):
        for point in self.points:
            if point not in edge.points:
                return point
        return None
    
    def getOppositeEdge(self, point):
        for edge in self.edges:
            if point not in edge.points:
                return edge
        return None
    
    def getDiagonalEdge(self):
        for edge in self.edges:
            if edge.isDiagonal:
                return edge
        return None
    
    def getTopmostPoints(self):
        # Returns topmost points (max .z) 
        result = []
        for point in self.points:
            if len(result) > 0:
                for topPoint in result:
                    if point.position.getZ() > topPoint.position.getZ():
                        result = [point]
                        break
                    elif point.position.getZ() == topPoint.position.getZ():
                        result.append(point)
                        break
            else:
                result = [point]
        return result
    
    def getCenter(self):
        center = Vec3(0,0,0)
        for point in self.points:
            center += point.position
        return center * 1.0/3.0
    
    def getFlux(self):
        return abs( self.area * self.normal.dot(ug) )
    
    def getOutflowDomain(self, direction):
        assert direction.length() > 0
        assert direction.cross(self.normal).length() > 0
        domain = {}
        
        # Calculate flow angle relative to the main vector
        uDir = Vec3(direction)
        uDir.normalize()
        angle = T.calculateAngle(self.directedEdges[0]["vector"], uDir, self.normal)
        angle = angle % (2*math.pi)
        
        # Find the domain
        iDomain = -1
        isEven = False
        while True:
            iDomain += 1
            isEven = not isEven
            
            domAngle2 = self.angularDomains[iDomain]
            if isEven:
                if angle >  domAngle2: continue
            else:
                if angle >= domAngle2: continue
            break
            
            if iDomain > 5:
                raise Exception("Error while finding domain for angle "+str(angle)+" in "+str(self.angularDomains))
            
        domAngle1 = self.angularDomains[iDomain-1] % (2*math.pi)
        domain["angle"] = angle - domAngle1
        domain["index"] = iDomain
        
        return domain
    
    def calculateAverageFlowLengthInLcs(self, coordsSystem):
        longestPath = 0.0
        origin = coordsSystem["origin"]
        
        # Calculate the y s of each point in lcs
        mapPosToY = map(lambda pos: (pos, T.getCoordsIn2DBasis(coordsSystem, pos)[1]), [point.position for point in self.points])
        mapPosToY = sorted(mapPosToY, None, lambda couple: couple[1])
        
        otherPointYs = filter(lambda couple: not couple[0].almostEqual(origin), mapPosToY)
        y1,y2 = otherPointYs[0][1],otherPointYs[1][1]
        
        if y1*y2 == 0:
            # Calculate the longest flow path lI
            alignedPositions = filter(lambda couple: couple[1] == 0, mapPosToY)
            longestPath = (alignedPositions[0][0] - alignedPositions[1][0]).length()

        else:
            # Calculate the longest flow path lI
            midPos = mapPosToY[1][0]
            midPoint = self.getPointByPosition(midPos)
            edgeOppositeToMid = self.getOppositeEdge(midPoint)
            midProjected = T.lineIntersectSegment(coordsSystem, midPos, self.slope, edgeOppositeToMid.getSegment())
            longestPath = (midPos - midProjected).length()
            
        return longestPath / 2.0
    
    def calculateAverageFlowLength(self):
        topmostPoints = self.getTopmostPoints() 
        x = self.slope
        y = self.normal.cross(self.slope)
        y.normalize()
        
        if len(topmostPoints) == 1:
            # Define the local coords system
            originPoint = topmostPoints[0]
            origin = originPoint.position # top point
            lcs = {"origin": origin, "x": x, "y": y}
            
            return self.calculateAverageFlowLengthInLcs(lcs);
        
        elif len(topmostPoints) == 2:
            # Define the local coords system
            for point in self.points:
                for tmp in topmostPoints:
                    if point != tmp:
                        origin = point.position # bottom point
                        
            lcs = {"origin": origin, "x": x, "y": y}
            
            return self.calculateAverageFlowLengthInLcs(lcs);  
        
        elif len(topmostPoints) == 3:
            # Flat triangle
#             return 0
            raise Exception("Error: encountered a flat triangle while calculating average flow length.")
    
    def projectDirection(self, edgeAngle, direction):
        projectedDir = (self.normal.cross(direction)).cross(self.normal)
        if edgeAngle < math.pi/2 or edgeAngle > 3*math.pi/2:
            projectedDir = projectedDir*(-1)
        projectedDir.normalize()
         
        return projectedDir

# Average flow speed calculator helper functions (obsolete)
#     def calculateSpeed(self, w0, wI, lI):
#         cosTheta = self.slope.dot(ug)
#         if w0 == None:
#             return self.slope * (1.0/self.area) * (8/15) * math.sqrt( gravity/cosTheta ) * lI * pow(wI, 1.5)
#         else:
#             return self.slope * (1.0/self.area) * (8/15) * math.sqrt( gravity/cosTheta ) * lI * ( pow(wI, 1.5) - (pow(w0, 2.5)-pow(wI, 2.5))/(w0-wI) )
#         
#     def calculateFlowAvgSpeedInLcs(self, coordsSystem):
#         origin = coordsSystem["origin"]
#         
#         # Calculate the y s of each point in lcs
#         mapPosToY = map(lambda pos: (pos, T.getCoordsIn2DBasis(coordsSystem, pos)[1]), [point.position for point in self.points])
#         mapPosToY = sorted(mapPosToY, None, lambda couple: couple[1])
#         
#         otherPointYs = filter(lambda couple: not couple[0].almostEqual(origin), mapPosToY)
#         y1,y2 = otherPointYs[0][1],otherPointYs[1][1]
#         
#         if y1*y2 == 0:
#             # Calculate the longest flow path lI
#             alignedPositions = filter(lambda couple: couple[1] == 0, mapPosToY)
#             lI = (alignedPositions[0][0] - alignedPositions[1][0]).length()
#             
#             yI = filter(lambda couple: couple[1] != 0, mapPosToY)[0][1]
#             
#             return self.calculateSpeed(None, yI, lI)
#         
#         else:
#             # Calculate the longest flow path lI
#             midPos = mapPosToY[1][0]
#             edgeOppositeToMid = self.getOppositeEdge(midPos)
#             midProjected = T.lineIntersectSegment(coordsSystem, midPos, self.slope, edgeOppositeToMid.getSegment())
#             lI = (midPos - midProjected).length()
#         
#             if y1*y2 > 0: 
#                 yI = min( abs(couple[1]) for couple in otherPointYs )
#                 y0 = max( abs(couple[1]) for couple in otherPointYs )
# 
#                 return self.calculateSpeed(y0, yI, lI)
#             
#             elif y1*y2 < 0: 
#                 yI = abs( min(couple[1]  for couple in otherPointYs ))
#                 y0 = sum( abs(couple[1]) for couple in otherPointYs )
#                 
#                 return self.calculateSpeed(y0, yI, lI)  
#             
#         return Vec3(0,0,0)
#                 
#     def calculateFlowAvgInitSpeed(self):
#         # Average of the speed function of all flow particles on the triangle.
#         topmostPoints = self.getTopmostPoints() 
#         x = self.slope
#         y = self.normal.cross(self.slope)
#         y.normalize()
#         
#         if len(topmostPoints) == 1:
#             # Define the local coords system
#             originPoint = topmostPoints[0]
#             origin = originPoint.position # top point
#             lcs = {"origin": origin, "x": x, "y": y}
#             
#             return self.calculateFlowAvgSpeedInLcs(lcs);        
#         
#         elif len(topmostPoints) == 2:
#             # Define the local coords system
#             for point in self.points:
#                 for tmp in topmostPoints:
#                     if point != tmp:
#                         origin = point.position # bottom point
#                         
#             lcs = {"origin": origin, "x": x, "y": y}
#             
#             return self.calculateFlowAvgSpeedInLcs(lcs);  
#         
#         elif len(topmostPoints) == 3:
#             return Vec3(0,0,0)
    
    def updateGeometry(self):
        self._updateVectors()
        self._computeAngularDomains()
    
    def isBoundary(self):
        return any(edge.isBoundary() for edge in self.edges)
    
    def isInLake(self, aany = False):
        isInLake = (point.isInLake() for point in self.points)
        return any(isInLake) if aany else all(isInLake)
    
    def isBedrock(self):
        return all(point.isBedrock() for point in self.points)
    
    def hasFlow(self, flowName):
        members = flowName.split(':')
        length = len(members)
        for i in range(0, length):
            ancestorFlowName = ':'.join(members[:length-i])
            if ancestorFlowName in self.flowNames:
                return True
        return False

    @staticmethod
    def getCommonEdge(tri1, tri2):
        edges1 = set(tri1.edges)
        edges2 = set(tri2.edges)
        intersection = edges1.intersection(edges2)
        return list(intersection)[0]

class Edge:
    '''
    Undirected edge defined by two points
    '''
    
    def __init__(self, point1, point2, autoconnect = True):
        self.points = set([point1, point2])
        if len(self.points) < 2:
            raise Exception("Duplicate point in Edge creation.")
        self.points = list(self.points)
        self.points.sort(key=lambda pnt: Vec3.length(pnt.position))
        self.points = tuple(self.points)
        self.name = point1.name + '-' + point2.name
        self.length = Vec3.length(point2.position - point1.position)
        self.triangles = () # Pointers to triangles sharing this edge
        self.angle = None # Angle at the edge, underneath the surface of the mesh (relative to gravity)
        self.isConcave = True # if angle > pi
        self.isDiagonal = False
        self.flow = 0.0 # A flow gets added here if a triangle repeats in the current flow branch
        self.flowDirection = "" # name of the point where the flow is going
        self.avgFlowVelocity = Vec3(0,0,0)
        self.lowerPoint = None
        
        # Calculate slope
        vector = self.points[0].position - self.points[1].position
        gravityDot = vector.dot(ug)
        if gravityDot < 0:
            vector *= -1
            vector.normalize()
        elif gravityDot == 0:
            vector *= 0
        elif gravityDot > 0:
            vector.normalize()
        self.slope = vector
        self.cosTheta = self.slope.dot(ug)
        self.updateLowerPoint()
            
        if autoconnect == True:
            self.connect()
    
    def __repr__(self):
        return ("Edge('" + self.name
                         + "', length=" + str(self.length)
                + ")")
    
    def __contains__(self, position):
        position0 = T.roundVec(self.points[0].position)
        position1 = T.roundVec(self.points[1].position)
        edgeVec1 = position1 - position0
        edgeVec1.normalize()
        
        edgeVec2 = edgeVec1 * -1.0
        
        testVec1 = position1 - position
        testVec1.normalize()
        
        testVec2 = position0 - position
        testVec2.normalize()
        
        dot1 = edgeVec1.dot(testVec1)
        dot2 = edgeVec2.dot(testVec2)
        
        if Vec3.lengthSquared(testVec1) == 0.0 or Vec3.lengthSquared(testVec2) == 0.0:
            # position is very close to one of the points
            return True
        elif position in self.getSegment():
            # position is the position of one of the points
            return True
        elif dot1 > 0 and dot2 > 0 and abs(dot1-1.0) < 10e-3:
            return True
        else:
            print dot1, dot2, position, self.points[1], self.points[0], 
            print edgeVec1, edgeVec2, testVec1, testVec2
            return False
    
    def connect(self):
        for point in self.points:
            point.addEdge(self)
            
    def addTriangle(self, triangle):
        triangles = list(self.triangles)
        triangles.append(triangle)
        self.triangles = tuple(triangles)    
    
    def calculateAngle(self):
        normal1 = Vec3(self.triangles[0].normal)
        normal2 = Vec3(self.triangles[1].normal)
        normal1.normalize()
        normal2.normalize()
        edgeNormal = normal1 + normal2
        edgeNormal.normalize()
        normalDot = normal1.dot(normal2)
        normalDot = round(normalDot, approximation)
        normalAngle = math.acos(normalDot)
        
        # To know if the edge is convex or concave
        # pick a point opposite to the edge 
        # create a vector pointing towards it 
        # and dot it with the normal
        
        point = self.triangles[0].getOppositePoint(self)
        vector = point.position - self.points[0].position
        dot = vector.dot(edgeNormal)
        edgeAngle = math.pi
        if dot > 0: # concave
            edgeAngle = math.pi + normalAngle
            self.isConcave = True
        elif dot < 0: # convex
            edgeAngle = math.pi - normalAngle
        else: # dot = 0, flat
            edgeAngle = math.pi
            
        self.angle = edgeAngle
    
    def getNeighborTriangle(self, triangle):
        if len(self.triangles) == 2:
            for tri in self.triangles:
                if tri is not triangle:
                    return tri
            return None
        else:
            return None
    
    def getNormal(self):
        normal = Vec3(0,0,0)
        for triangle in self.triangles:
            normal += triangle.normal
        normal.normalize()
        
        return normal
    
    def getDownwardsVector(self):
        vector = self.points[0].position - self.points[1].position
        gravityDot = vector.dot(ug)
        if gravityDot < 0:
            vector *= -1
            vector.normalize()
        elif gravityDot == 0:
            vector *= 0
        elif gravityDot > 0:
            vector.normalize()
                        
        return vector
    
    def getVectorStartingAt(self, point):
        for targetPoint in self.points:
            if targetPoint != point:
                vec = targetPoint.position - point.position
                vec.normalize()
                return vec
        raise Exception
        
    def getOtherPoint(self, pointName):
        assert isinstance(pointName, str) 
        for somePoint in self.points:
            if somePoint.name != pointName:
                return somePoint
        return None
    
    def getPointByName(self, pointName):
        assert isinstance(pointName, str) 
        for somePoint in self.points:
            if somePoint.name == pointName:
                return somePoint
        return None
    
    def getCenter(self):
        center = Vec3(0,0,0)
        for point in self.points:
            center += point.position
        center *= 0.5
        
        return center
        
    def getSegment(self):
        return [point.position for point in self.points]
        
    def updateFlowDirection(self, dir):
        edgeVec = self.points[1].position - self.points[0].position
        dot = edgeVec.dot(dir)
        if dot > 0:
            self.flowDirection = self.points[1].name
        elif dot < 0:
            self.flowDirection = self.points[0].name
        else:
            self.flowDirection = ""
        
    def updateLowerPoint(self):
        if   self.points[0].position.getZ() > self.points[1].position.getZ():
            self.lowerPoint = self.points[1]
        elif self.points[0].position.getZ() < self.points[1].position.getZ():
            self.lowerPoint = self.points[0]
        else: self.lowerPoint = None
        
    def updateGeometry(self):
        self.length = Vec3.length(self.points[1].position - self.points[0].position)
        if len(self.triangles) == 2:
            self.calculateAngle()
        self.updateLowerPoint()
        
    def isBoundary(self):
        return all(point.isBoundary() for point in self.points)
    
    def isInLake(self):
        return all(point.isInLake() for point in self.points)
    
    def isBedrock(self):
        return all(point.isBedrock() for point in self.points)
        
    @staticmethod
    def getCommonPoint(edge1, edge2):
        points1 = set(edge1.points)
        points2= set(edge2.points)
        intersection = points1.intersection(points2)
        return list(intersection)[0]

class Point:
    '''
    Point defined by three coordinates
    '''
    
    maxDeltaHeight = EROSION_DEPTH_FACTOR * 10**3 # max height difference for one iteration of sediment transport
    
    def __init__(self, name, x, y, z, bedrockDepth = None):
        self.name = name
        self.heights = [z] #heights per iteration
        self.delta = 0
        self.position = Vec3(x, y, z)
        self.avgFlowVelocity = Vec3(0,0,0)
        self.avgFlowVelocities = []
        self.flow = 0
        self.flows = []
        self.drainageBasinIds = []
        self.drainageBasinId = -1
        self.edges = () # Pointers to edges sharing this point
        self.lowestNeighbors = [] # neighboring point of the lowest height
        if bedrockDepth is None:
            self.bedrockDepth = z - BEDROCK_DEPTH
        else:
            self.bedrockDepth = bedrockDepth
        self.hasEroded = False
        
    def __repr__(self):
        return ("Point('" + self.name
                          + "', (" + str(self.position.getX())
                          +  ", " + str(self.position.getY())
                          +  ", " + str(self.position.getZ())
                + "))")
        
    def addEdge(self, edge):
        edges = list(self.edges)
        edges.append(edge)
        self.edges = tuple(edges)
        
    def updateLowestNeighbor(self):
        neighbors = self.getNeighbors()
        height = self.position.getZ()
        self.lowestNeighbors = []
        
        for point in neighbors:
            if point.position.getZ() < height:
                height = point.position.getZ()
                self.lowestNeighbors = [point,]
            elif point.position.getZ() == height:
                self.lowestNeighbors.append(point)
        
    def getSlope(self):
        slope = Vec3(0,0,0)
        for edge in self.edges:
            slope += edge.slope
        slope.normalize()
        
        return slope
    
    def getNormal(self):
        normal = Vec3(0,0,0)
        for edge in self.edges:
            normal += edge.getNormal()
        normal.normalize()
        
        return normal   
    
    def getNeighbors(self):
        neighbors = []
        for edge in self.edges:
            for point in edge.points:
                if point is not self:
                    neighbors.append(point)
        return neighbors
    
    def getNeighborTriangles(self):
        neighbors = []
        for edge in self.edges:
            for tri in edge.triangles:
                neighbors.append(tri)
        neighbors = set(neighbors)
        return tuple(neighbors)
    
#     def getAdmissibleDeltaHeight(self, deltaH):
#         avgNeighborHeight = self.getAvgNeighborHeight()
#         futureHeight = self.position.getZ() + self.deltaPosition.getZ()
#         relativeNeighborDelta = (avgNeighborHeight - futureHeight) / Point.maxDeltaHeight
#         
#         if deltaH > 0:
#             if relativeNeighborDelta < 0:
#                 return 0.
#             else:
#                 return deltaH * relativeNeighborDelta
#         
#         if deltaH < 0:
#             if relativeNeighborDelta > 0:
#                 return 0.
#             else:
#                 return deltaH * abs(relativeNeighborDelta)
    
    def getAdmissibleDeltaHeight(self, deltaH, checkCurrentDelta = False):
        avgNeighborHeight = self.getAvgNeighborHeight()
        currentHeight = self.position.getZ() if not checkCurrentDelta else self.position.getZ() + self.deltaPosition.getZ()
                
        if deltaH > 0:
            # Deposition
            if currentHeight <= avgNeighborHeight:
                return deltaH
            else:
                return deltaH*math.exp(-deltaH)
        
        if deltaH < 0:
            # Erosion
            if currentHeight >= avgNeighborHeight:
                return deltaH
            else:
                return deltaH*math.exp(deltaH)
                
        return deltaH
    
#     def getAdmissibleDeltaHeight(self, deltaH):
#         avgNeighborHeight = self.getAvgNeighborHeight()
#         currentHeight = self.position.getZ() + self.deltaPosition.getZ()
#          
#         if abs(avgNeighborHeight - (currentHeight + deltaH)) < abs(avgNeighborHeight - currentHeight):
#             return deltaH
#         else:
#             if deltaH * (avgNeighborHeight - currentHeight) > 0:
#                 # If the variation goes in the right direction
#                 return 2*(avgNeighborHeight - currentHeight)
#             else:
#                 return 0.
    
#     def getAdmissibleDepositionHeight(self, deltaH):
#         maxDeltaHeight = self.getOptDeltaHeight()
#         currentDeltaH = self.deltaPosition.getZ()
#         
#         if 0 < currentDeltaH + deltaH < maxDeltaHeight:
#             return deltaH
#         elif 0 < currentDeltaH < maxDeltaHeight:
#             if deltaH > 0:
#                 return maxDeltaHeight - currentDeltaH
#             else:
#                 return currentDeltaH
#         elif currentDeltaH < 0:
#             if deltaH > 0:
#                 if currentDeltaH + deltaH < 0:
#                     return delta
#                 return maxDeltaHeight - currentDeltaH
#             else:
#                 return 0.
#             
        
#     def getBoundedDeltaHeight(self, deltaH):
#         maxDeltaHeight = self.getOptDeltaHeight()
#         currentDeltaH = self.deltaPosition.getZ()
#         
# #         if currentDeltaH*maxDeltaHeight > 0:
# #             if abs(currentDeltaH) > abs(maxDeltaHeight):
# #                 return 0.
#         if 0 < abs(currentDeltaH + deltaH) < abs(maxDeltaHeight):
#             return deltaH
#         else:
#             if currentDeltaH*maxDeltaHeight > 0:
#                 
#             
#             
#             if 0 < abs(currentDeltaH) < abs(maxDeltaHeight):
#                 if deltaH*maxDeltaHeight > 0:
#                     return maxDeltaHeight-currentDeltaH
#                 else:
#                     return currentDeltaH
#             elif maxDeltaHeight < :
#                 
#         
#         
#         if currentDeltaH > maxDeltaHeight:
#             return 0.
#         else:
#             if abs(currentDeltaH + deltaH) < maxDeltaHeight:
#                 return deltaH
#             else:
#                 if abs(currentDeltaH) > maxDeltaHeight:
#                     return 0.
#                 else:
#                     return math.copysign(maxDeltaHeight - abs(currentDeltaH), deltaH)
    
    def isLocalMin(self):
        neighbors = self.getNeighbors()
        for point in neighbors:
            if point.position.getZ() < self.position.getZ():
                return False
        return True
    
    def isBoundary(self):
        # If the point is at the boundary of the world
        return len(self.edges) % 4 != 0
    
    def isInLake(self):
        return (self.drainageBasinId in D.basinsWithLakes and 
                self.position.getZ() <= D.basinHeads[self.drainageBasinId]["height"])
    
    def isBedrock(self):
        return self.heights[-1] == self.bedrockDepth
        
    def getAvgNeighborHeight(self):
        avgNeighborHeight = 0.
        neighbors = self.getNeighbors()
        for point in neighbors:
            avgNeighborHeight += point.position.getZ() + point.deltaPosition.getZ()
#             if point.position.getZ() < self.position.getZ():
#                 return Point.maxDeltaHeight
#         return max(Point.maxDeltaHeight, avgNeighborHeight / len(neighbors))
        avgNeighborHeight /= len(neighbors)
        return avgNeighborHeight
    
    def getMaxNeighborHeight(self):
        maxNeighborHeight = -float("inf")
        neighbors = self.getNeighbors()
        for point in neighbors:
            maxNeighborHeight = max(maxNeighborHeight, point.position.getZ())# + point.deltaPosition.getZ()
        
        return maxNeighborHeight
    
    def getMinNeighborHeight(self):
        minNeighborHeight = float("inf")
        neighbors = self.getNeighbors()
        for point in neighbors:
            minNeighborHeight = min(minNeighborHeight, point.position.getZ())# + point.deltaPosition.getZ()
        
        return minNeighborHeight

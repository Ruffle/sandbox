'''
Created on 4 nov. 2017

@author: Rafael
'''
from panda3d.core import Vec3
import Tools as T


basinHeads = {}# map int to {"height": float, "points": [ECG.Point,], "flow": float, "velocity": Vec3}
basinsWithLakes = set()
basinBottoms = {}
basinPoints = {}
basinSediment = {}
basinMap = {}

externalBasinEntraces = set() # Points where the fluid leaves the world and drains into an external basin

class SearchProblem(object):
    """
    Represents the definition of a drainage basin as a search problem
    which can be solved by classical search algorithms.
    """

    def __init__(self, bottom, basinId):
        self.bottom  = bottom
        self.basinId = basinId
        self.allPoints = []
    
    def getStartNode(self):
        """
        Returns the start state for the search problem:
        The bottom of a drainage basin.
        """
        return (self.bottom, self.bottom.position.getZ())

    def isGoalState(self, state):
        """
          state: An ECG.Point

        Never returns true since we just want to propagate the value
        """
        return False

    def getSuccessors(self, state):
        """
          state: Search state
        
        For a given state, this should return a list of triples, (successor,
        stepCost), where 'successor' is a successor to the current
        state and 'stepCost' is the incremental cost of expanding to that successor.
        """
        successors = []
        if state.drainageBasinId == -1:
            self.allPoints.append(state)
            state.drainageBasinId = self.basinId
            if not state.isBoundary():
                successors = [(n, n.position.getZ()) for n in state.getNeighbors()]
                successors = filter(lambda (n, h): state in n.lowestNeighbors, successors)

#                     for edge in state.edges:
#                         if edge.lowerPoint == state or edge.lowerPoint == None:
#                             successorPoint = edge.getOtherPoint(state.name)
#                             successorHeight = successorPoint.position.getZ()
#                             successors.append( (successorPoint, successorHeight) )
        return successors

class HeadSearchProblem(SearchProblem):
    """
    Represents the definition of a lake's outlet(s) as a search problem
    which can be solved by classical search algorithms.
    """
#         def isGoalState(self, state):
#             """
#               state: An ECG.Point
#     
#             Returns True if and only if the state is a valid goal state
#             """
#             return any(edge.lowerPoint.drainageBasinId != self.basinId for edge in state.edges)
    def getSuccessors(self, state):
        """
          state: Search state
        
        For a given state, this should return a list of triples, (successor,
        stepCost), where 'successor' is a successor to the current
        state and 'stepCost' is the incremental cost of expanding to that successor.
        """
        successors = []
        self.allPoints.append(state)
        if not state.isBoundary():
#             neighBasins = [neigh.drainageBasinId for neigh in state.lowestNeighbors]
#             for id in neighBasins:
#                 if id != self.basinId and not isSubBasin(id, self.basinId):
#                     return successors
#             successors = [(n, n.position.getZ()) for n in state.getNeighbors()]
#             successors = filter(lambda (n, h): state in n.lowestNeighbors, successors)
            for edge in state.edges:
                id = edge.lowerPoint.drainageBasinId if edge.lowerPoint is not None else -1
                if edge.lowerPoint == state or edge.lowerPoint == None:
                    successorPoint = edge.getOtherPoint(state.name)
                    successorHeight = successorPoint.position.getZ()
                    successors.append( (successorPoint, successorHeight) )
                elif id != self.basinId and not isSubBasin(id, self.basinId):
                    successors = []
                    break
#         print successors
        return successors

class UpdateGeomSearchProblem(SearchProblem):
    """

    """
    def __init__(self, bottom, minHeightDifference, basinId = -1):
        self.basinId = basinId
        self.bottom = bottom
        self.minHeightDifference = minHeightDifference
        self.edgesToUpdate = set()
        self.trisToUpdate = set()
    
    def getSuccessors(self, state):
        """
          state: Search state
        
        For a given state, this should return a list of triples, (successor,
        stepCost), where 'successor' is a successor to the current
        state and 'stepCost' is the incremental cost of expanding to that successor.
        """
        self.updatePoint(state)
        successors = []
        successors = [(n, n.position.getZ()) for n in state.getNeighbors()]
        successors = filter(lambda (n, h): state in n.lowestNeighbors, successors)
        successors = filter(lambda (n, h): n.drainageBasinId == self.basinId, successors)
        
        return successors
    
    def getAdmissibleDelta(self, point):
        height = point.position.getZ()
        neighHeights = [n.position.getZ() for n in point.lowestNeighbors]
        neighHeights = filter(lambda h: h < height, neighHeights)
        if len(neighHeights) > 0:
            maxLowestNeighHeight = max(neighHeights)
            admissibleDeltaHeight = maxLowestNeighHeight - height + self.minHeightDifference
            return admissibleDeltaHeight
        else:
            return point.delta
    
    def updatePoint(self, point):
        if point.delta != 0:
#             """
            if point.drainageBasinId != -1:
                if point.isInLake() and point.delta >= 0:
                    # Lake water height cannot be exceeded for deposition
                    admissibleDeltaHeight = basinHeads[self.basinId]["height"] - point.position.getZ()
                    assert admissibleDeltaHeight >= 0
                    point.position += Vec3.unitZ() * min(point.delta, admissibleDeltaHeight)
                else:
                    # There can be no erosion below lake water height
                    point.position += Vec3.unitZ() * min(0, max(point.delta, self.getAdmissibleDelta(point)))#point.delta#
            else:
                # Enforce drainage consistency
                point.position += Vec3.unitZ() * min(0, max(point.delta, self.getAdmissibleDelta(point)))#point.delta#

#             point.position += Vec3.unitZ() * point.delta
            point.delta = 0
            
            self.edgesToUpdate.update(point.edges)
            for edge in point.edges:
                self.trisToUpdate.update(edge.triangles)

def getRootBasin(subId):
    id = basinMap[subId]
    if id in basinMap:
        return getRootBasin(id)
    else:
        return id

def isSubBasin(subId, id):
    if subId in basinMap:
        if basinMap[subId] == id: return True
        if basinMap[subId] in basinMap: 
            return isSubBasin(basinMap[subId], id)
    return False
    
def defineBasin(value, startPoint):
    expandedPoints = set()
    lowestPoint = startPoint
    while True:
        if lowestPoint.drainageBasinId != -1:
            return
        if lowestPoint in expandedPoints: # Case of several neighboring points all being bottoms of a basin
            break
        if len(lowestPoint.lowestNeighbors) == 0: # Case of one point being bottom of a basin
            break
        expandedPoints.add(lowestPoint)
        lowestPoint = lowestPoint.lowestNeighbors[0]
        
    if not lowestPoint.isBoundary():
        basinsWithLakes.add(value)
        
        # Propagate the value upflow
        sp = SearchProblem(lowestPoint, value)
        basinPoints[value] = sp.allPoints
        leaves = T.uniformCostSearch(sp)
        if len(leaves) > 0 and len(sp.allPoints) > 0:
            # Check if the current basin is inside another basin, two cases:
            # 1. Current basin entirely contained in another basin
            # 2. Current basin partialy contained in another basin and the world's boundary
            outerBasinId = None
            outerBasinIds = list(set(leaf[0].drainageBasinId for leaf in leaves))
            outerBasinIds = filter(lambda id: id != value, outerBasinIds)
            if len(outerBasinIds) > 0:
                outerBasinId = outerBasinIds[0]
            
            if outerBasinId is not None:
                basinMap[value] = outerBasinId
        
        # Update bottoms
        if value in basinBottoms:
            if lowestPoint.position.getZ() < basinBottoms[value]:
                basinBottoms[value] = lowestPoint
        else:
            basinBottoms[value] = lowestPoint
            
    else:
        # Add -1 bottom
        externalBasinEntraces.add(lowestPoint)
#         if -1 in basinBottoms and lowestPoint.position.getZ() < basinBottoms[-1]:
#             basinBottoms[-1] = lowestPoint
#         else:
#             basinBottoms[-1] = lowestPoint
            
def findHeads():
    for basinId, bottom in basinBottoms.iteritems():
        # Find heads
        hsp = HeadSearchProblem(bottom, basinId)
        leaves = T.uniformCostSearch(hsp)
        
        # Take the heads and associate them with the basin id
        i = 0
        leaf = leaves[i]
        lakeHeight = leaf[1]
        lakeHeads = {"height": lakeHeight, "points": []}
        while leaf[1] == lakeHeight:
            lakeHeads["points"].append(leaf[0])
            if i == len(leaves)-1: break
            i += 1
            leaf = leaves[i]
        
        # Update heads
        if basinId in basinHeads:
            if lakeHeight < basinHeads[basinId]["height"]:
                basinHeads[basinId] = lakeHeads
        else:
            basinHeads[basinId] = lakeHeads
            
    for basinId, head in basinHeads.iteritems():
#         print basinId, head
        head["flow"] = 0
        head["velocity"] = 0
            
def manageSubBasins():
    for subId, id in sorted(basinMap.iteritems(), reverse=True):
        outerBasinHead = basinHeads[id]
        innerBasinHead = basinHeads[subId]
        
        if outerBasinHead["height"] >= innerBasinHead["height"]:
            for point in basinPoints[subId]: 
                point.drainageBasinId = id #getRootBasin(subId)
            basinPoints[id] += basinPoints[subId]

def addSediment(basinId, value):
    if not basinId in basinSediment:
        basinSediment[basinId] = value
    else:
        basinSediment[basinId] += value

def isHead(point):
    if point.drainageBasinId in basinHeads:
        if point in basinHeads[point.drainageBasinId]["points"]:
            return True
    return False
'''
Created on 14/02/2016

@author: Rafal Starzyk
'''

import Tools as T
from App import App
from World import World

A = App()

A.createWorld()
A.world.addSection(size = 100.0, 
             numIterations = 1, 
             law = "uniform", 
             heightVariability = 0, 
             roughness = 1.5,
             colors = T.drainage,
             randomWeights = False,
             )
# A.world.addPlanet(radius = 100.0, 
#             numIterations = 8, 
#             law = "triangular", 
#             heightVariability = 1.0, 
#             roughness = 1.3,
#             colors= T.moon,
#             numCraters = 500,
#             randomWeights = False,
#             )
A.run()

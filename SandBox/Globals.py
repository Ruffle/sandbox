'''
Created on 21/12/2016

@author: Rafael
'''
import math
import time
from panda3d.core import Vec3


# Physical properties
MASS_PER_VOLUME_SOLID = 2650.0 # in kg / m**3
MASS_PER_VOLUME_FLUID = 1000.0 # in kg / m**3
VISCOSITY             = 0.001 # primary, in kg /(m * s)

# Speed calculation factors
KG = 10**0
KV = 10**0

GRAVITY = 9.81
ug = Vec3(0,0,-1)
g = Vec3(0,0,-GRAVITY)
approximation = 5

# Erosion/deposition velocity calculation (Natural Grains: Sieve Diameters)

C1 = 18.0
C2 = 1.0

R = (MASS_PER_VOLUME_SOLID - MASS_PER_VOLUME_FLUID) / MASS_PER_VOLUME_FLUID # submerged specific gravity
Nu = VISCOSITY / MASS_PER_VOLUME_FLUID # kinematic viscosity, in m**2 / s
AVG_GRAIN_SIZE = 0.01 # in m
GRAIN_SIZE_DISPERSION = 10
MIN_GRAIN_SIZE = AVG_GRAIN_SIZE / GRAIN_SIZE_DISPERSION
MAX_GRAIN_SIZE = AVG_GRAIN_SIZE * GRAIN_SIZE_DISPERSION

SEDIMENT_VOLUME_INTEGRAL_RIGHT_HALF = math.pi * (MAX_GRAIN_SIZE**5 / 20.0 + AVG_GRAIN_SIZE**5 / 5.0 - AVG_GRAIN_SIZE**4 * MAX_GRAIN_SIZE / 4.0) \
                            / ((AVG_GRAIN_SIZE - MIN_GRAIN_SIZE)**2 * 3.0)
SVI = lambda Dmin,Dmax: math.pi * ( (Dmax**5 - Dmin**5) / 5.0 - MIN_GRAIN_SIZE * (Dmax**4 - Dmin**4) / 4.0 ) \
                            / ((AVG_GRAIN_SIZE - MIN_GRAIN_SIZE)**2 * 6.0)
SEDIMENT_VOLUME_INTEGRAL_TOTAL = SVI(MIN_GRAIN_SIZE, AVG_GRAIN_SIZE) + SEDIMENT_VOLUME_INTEGRAL_RIGHT_HALF

EROSION_SPEED_FACTOR = 5.0 # how much bigger is eroding velocity relative to settling velocity
EROSION_DEPTH_FACTOR = 10e-4

Vd = lambda D: R * GRAVITY * D**2 / (C1 * Nu + math.sqrt(0.75 * C2 * R * GRAVITY * D**3)) # deposition velocity
Dd = lambda V: 0.5 * (MIN_GRAIN_SIZE*V/Vdmin + AVG_GRAIN_SIZE*V/Vd0) # cba to do better approximation
Vdmin = Vd(MIN_GRAIN_SIZE)
Vd0 = 5*Vd(AVG_GRAIN_SIZE)
Ve0 = EROSION_SPEED_FACTOR * Vd0 # erosion velocity

# Kinetic energy calculation
KINETIC_ENERGY_FACTOR = 1.0
Ek = lambda f,v: KINETIC_ENERGY_FACTOR * MASS_PER_VOLUME_FLUID * f * v**2
EkdMin = Ek(10.0, Vdmin)
Ekd0 = Ek(10.0, Vd0)
Eke0 = Ek(10.0, Ve0)

# Factors for different kinds of erosion
RIEF  = 10**1 # RAINFALL_IMPACT_EROSION_FACTOR
REF   = 10**-3 # RIVERBED_EROSION_FACTOR
FSVEF = 0 #10**0 # FLOW_SPEED_VARIATION_EROSION_FACTOR

# print "EROSION VELOCITY = ", Ve0
BEDROCK_DEPTH = 10.0

#Drainage
MIN_POINT_HEIGHT_DIFFERENCE = 1
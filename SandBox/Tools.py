'''
Created on 21/02/2016

@author: Rafal Starzyk
'''

from cmath import pi
import cmath
import math
import heapq
from panda3d.core import Vec3, GeomNode, GeomVertexFormat, GeomVertexData, \
    Geom, GeomVertexWriter, GeomLines, GeomTrifans, LQuaternionf, Vec2
# import CTools as ct
import numpy as np
import cProfile, pstats, StringIO
from Globals import GRAVITY

approximation = 5
tolerance = 1e-3

# COLORS
RED     = Vec3(1,0,0)
YELLOW  = Vec3(1,1,0)
GREEN   = Vec3(0,1,0)
TEAL    = Vec3(0,1,1)
BLUE    = Vec3(0,0,1)
PURPLE  = Vec3(1,0,1)

WHITE   = Vec3(1,1,1)
GREY    = Vec3(0.5,0.5,0.5)
BLACK   = Vec3(0,0,0)

DARK_GREEN = Vec3(0,0.2,0)

SHALLOW_BLUE    = Vec3(0.565, 0.922, 0.760)
OCEAN_BLUE      = Vec3(0.145, 0.192, 0.475)
DEEP_BLUE       = Vec3(0.035, 0.039, 0.220)
PLANT_GREEN     = Vec3(0.200, 0.260, 0.129)
LIGHT_BROWN     = Vec3(0.780, 0.659, 0.478)
MOUNTAIN_BROWN  = Vec3(0.231, 0.220, 0.090)


white = ({"normHeight": 0.0, "type": "elevated", "-value": WHITE, "+value": WHITE}, 
         {"normHeight": 1.0, "type": "elevated", "-value": WHITE, "+value": WHITE})

defaultColors = ({"normHeight": 0.0, "type": "elevated", "-value": BLUE,  "+value": BLUE}, 
                 {"normHeight": 0.5, "type": "elevated", "-value": GREEN, "+value": GREEN}, 
                 {"normHeight": 1.0, "type": "elevated", "-value": RED,   "+value": RED})

drainage      = ({"normHeight": 0.0, "type": "elevated", "-value": RED,   "+value": RED}, 
                 {"normHeight": 0.5, "type": "elevated", "-value": GREEN, "+value": GREEN}, 
                 {"normHeight": 1.0, "type": "elevated", "-value": BLUE,  "+value": BLUE})

earth = ({"normHeight": 0.0, "type": "flat",     "-value": DEEP_BLUE, "+value": DEEP_BLUE}, 
         {"normHeight": 0.49,"type": "flat", "-value": OCEAN_BLUE, "+value": OCEAN_BLUE}, 
         {"normHeight": 0.5, "type": "elevated", "-value": SHALLOW_BLUE, "+value": DARK_GREEN}, 
         {"normHeight": 0.6, "type": "elevated", "-value": PLANT_GREEN, "+value": PLANT_GREEN}, 
         {"normHeight": 0.85, "type": "elevated", "-value": LIGHT_BROWN, "+value": LIGHT_BROWN},
         {"normHeight": 0.95, "type": "elevated", "-value": MOUNTAIN_BROWN, "+value": MOUNTAIN_BROWN},
         {"normHeight": 1.0, "type": "elevated", "-value": WHITE, "+value": WHITE})

redEarth = ({"normHeight": 0.0, "type": "flat",     "-value": Vec3(0,0,0.2), "+value": Vec3(0,0,0.2)}, 
            {"normHeight": 0.5, "type": "elevated", "-value": Vec3(0,0.6,0.8), "+value": Vec3(0.8,0.2,0)}, 
            {"normHeight": 1, "type": "elevated", "-value": Vec3(0.3,0,0), "+value": Vec3(0.3,0,0)})

moon  = ({"normHeight": 0.0, "type": "elevated", "-value": BLACK,  "+value": BLACK}, 
         {"normHeight": 1.0, "type": "elevated", "-value": GREY, "+value": GREY})

venus = ({"normHeight": 0.0, "type": "elevated", "-value": Vec3(0.157, 0.117, 0.000), "+value": Vec3(0.157, 0.117, 0.000)}, 
         {"normHeight": 0.3, "type": "elevated", "-value": Vec3(0.835, 0.592, 0.204), "+value": Vec3(0.835, 0.592, 0.204)},
         {"normHeight": 0.5, "type": "elevated", "-value": Vec3(0.584, 0.282, 0.071), "+value": Vec3(0.584, 0.282, 0.071)},
         {"normHeight": 0.8, "type": "elevated", "-value": Vec3(0.835, 0.592, 0.204), "+value": Vec3(0.835, 0.592, 0.204)},
         {"normHeight": 1.0, "type": "elevated", "-value": Vec3(1.000, 0.900, 0.557), "+value": Vec3(1.000, 0.900, 0.557)})

mars =  ({"normHeight": 0.0, "type": "elevated", "-value": BLACK, "+value": BLACK}, 
         {"normHeight": 0.3, "type": "elevated", "-value": Vec3(1, 0.64, 0.478), "+value": Vec3(0.77, 0.5, 0.329)},
         {"normHeight": 0.5, "type": "elevated", "-value": Vec3(0.588, 0.415, 0.318), "+value": Vec3(0.588, 0.415, 0.318)},
         {"normHeight": 0.8, "type": "elevated", "-value": Vec3(0.77, 0.5, 0.329), "+value": Vec3(0.77, 0.5, 0.329)},
         {"normHeight": 1.0, "type": "elevated", "-value": Vec3(1, 0.64, 0.478), "+value": Vec3(1, 0.64, 0.478)})

class Queue:
    "A container with a first-in-first-out (FIFO) queuing policy."
    def __init__(self):
        self.list = []

    def push(self,item):
        "Enqueue the 'item' into the queue"
        self.list.insert(0,item)

    def pop(self):
        """
          Dequeue the earliest enqueued item still in the queue. This
          operation removes the item from the queue.
        """
        return self.list.pop()

    def isEmpty(self):
        "Returns true if the queue is empty"
        return len(self.list) == 0

class PriorityQueue:
    """
      Implements a priority queue data structure. Each inserted item
      has a priority associated with it and the client is usually interested
      in quick retrieval of the lowest-priority item in the queue. This
      data structure allows O(1) access to the lowest-priority item.

      Note that this PriorityQueue does not allow you to change the priority
      of an item.  However, you may insert the same item multiple times with
      different priorities.
    """
    def  __init__(self):
        self.heap = []
        self.count = 0

    def push(self, item, priority):
        entry = (priority, self.count, item)
        heapq.heappush(self.heap, entry)
        self.count += 1

    def pop(self):
        (_, _, item) = heapq.heappop(self.heap)
        return item

    def isEmpty(self):
        return len(self.heap) == 0

class CircularList(list):
    '''
    classdocs
    '''

    def __getitem__(self, idx):
        length = len(self)
        if idx < 0:
            newIdx = idx % length - length
        else:
            newIdx = idx % length
        return list.__getitem__(self, newIdx)
    
class Vector:
    '''
    classdocs
    '''
    
    def __init__(self, name, vec, pos=Vec3(0,0,0), color=Vec3(1,1,1)):
        numVerticesOnCircle = 4
        length = vec.length()
        
        # Calculate quaternion
        vec.normalize()
        z = Vec3(0,0,1)
        cross = z.cross(vec)
        dot = z.dot(vec)
        quat = LQuaternionf(dot+1, cross.getX(), cross.getY(), cross.getZ())
        quat.normalize()
        
        node = GeomNode(name + ".node")
        self.anchor = render.attachNewNode(node)
        self.anchor.setTwoSided(True)
        self.anchor.setPos(pos)
        self.anchor.setQuat(quat)

        vformat = GeomVertexFormat.getV3c4()
        vdata = GeomVertexData(name + ".vdata", vformat, Geom.UHDynamic)
        vdata.setNumRows(2 + numVerticesOnCircle)

        vertexWriter = GeomVertexWriter(vdata, 'vertex')
        colorWriter = GeomVertexWriter(vdata, 'color')
        
        self.color = color
        
        vertexWriter.addData3f(0,0,0)
        colorWriter.addData3f(color)
        vertexWriter.addData3f(0,0,length)
        colorWriter.addData3f(color)
        
        coneRadius = 0.1*length
        v = complex(coneRadius,0)
        angle = 2*pi/float(numVerticesOnCircle)
        cangle = cmath.exp(angle*1j)
        
        for i in range(numVerticesOnCircle):
            vertexWriter.addData3f(v.real, v.imag, 0.75*length)
            colorWriter.addData3f(color)
            v *= cangle
            
        lineGeom = Geom(vdata)
        line = GeomLines(Geom.UHStatic)
        line.addVertices(0, 1)
        line.closePrimitive()
        lineGeom.addPrimitive(line)
        
        coneGeom = Geom(vdata)
        cone = GeomTrifans(Geom.UHStatic)
        cone.addVertex(1)
        for j in range(numVerticesOnCircle):
            cone.addVertex(2+j)
        cone.addVertex(2)
        cone.closePrimitive()
        coneGeom.addPrimitive(line)
                
        node.addGeom(lineGeom)
        node.addGeom(coneGeom)
        
def createAlternatingList( list1, list2):
    if abs( len(list1) - len(list2) ) > 1:
        raise Exception
    if isinstance(list1, CircularList) or isinstance(list2, CircularList):
        result = CircularList()
    else:
        result = []
        
    length = len(list1) + len(list2)
    j,k = 0,0
    for i in range(length):
        if i % 2 == 0:
            item = list1[j]
            j += 1
        else:
            item = list2[k]
            k += 1
        result.append(item)
        
    return result

def calculateTriArea(length, angle1, angle2):
    return 0.5 * length**2 * math.sin(angle1) * math.sin(angle1) / math.sin(angle1 + angle2)

def calculateTransportedVolume(triangle, direction):
    # Calculate the change in volume per unit length when the points of a triangle are uniformly displaced 
    # by a vector colinear to its normal, according to a direction which is either 1 or -1
    volume = 0.0
    
    # Add volume of the 3 pyramids
    for edge in triangle.edges:
        pyramidTop = triangle.getOppositePoint(edge)
        edgeVec    = edge.points[0].position - edge.points[1].position
        pyramidVec = edge.points[1].position - pyramidTop.position
        volume += abs( edgeVec.cross(Vec3.unitZ()*direction).dot(pyramidVec) ) / 3.0
    
    # Add volume of the tetrahedrons
    forbiddenTriangles = [triangle]
    forbiddenTriangles.extend( edge.getNeighborTriangle(triangle) for edge in triangle.edges) # triangles adjacent to the 
    for point in triangle.points:
        tetraTriangles = []
        for edge in point.edges:
            for triangle in edge.triangles:
                if triangle not in forbiddenTriangles:
                    tetraTriangles.append(triangle)
        tetraTriangles = tuple(frozenset(tetraTriangles))
        
        for triangle in tetraTriangles:
            edge = triangle.getOppositeEdge(point)
            edgeVec  = edge.points[0].position - edge.points[1].position
            tetraVec = edge.points[1].position - point.position
            volume += abs( tetraVec.cross(Vec3.unitZ()*direction).dot(edgeVec) ) / 6.0
    
    return volume

def limitSedimentDepth(tri, depth, action):
    # action = "min" or "max"
    correctedDepth = depth
    for point in tri.points:
        neighbors = point.getNeighbors()
        pointHeights = {}
        for neighbor in neighbors:
            if neighbor in tri.points:
                pointHeights[neighbor.name] = neighbor.position.getZ() - depth if action == "min" else neighbor.position.getZ() + depth
            else:
                pointHeights[neighbor.name] = neighbor.position.getZ()
        extremeHeight = min(pointHeights.values()) if action == "min" else max(pointHeights.values())
        correctedDepth = min(abs(point.position.getZ() - extremeHeight), correctedDepth)
        
    return correctedDepth
    
def calculateAngle(vec1, vec2, orientation):
    # vec1 and vec2 are assumed to be normalized
    uVec2Proj = vec2 - vec2.project(orientation)
    angle = vec1.signedAngleRad(uVec2Proj, orientation)
    if math.isnan(angle):
        raise Exception("Angle between {0} and {1}, with reference {2} is NaN.".format(vec1, vec2, orientation))
    angle = angle % (2*math.pi)
    
    return angle

def getCoordsIn2DBasis(coordinateSystem, vector):
    return ((vector - coordinateSystem["origin"]).dot(coordinateSystem["x"]),
            (vector - coordinateSystem["origin"]).dot(coordinateSystem["y"]))
    
def lineIntersectSegment(coordinateSystem, linePointPosition, lineVector, segment):
    lineIntersectSegment.numCalls += 1
    o = coordinateSystem["origin"]
    x = coordinateSystem["x"]
    y = coordinateSystem["y"]
    
    # Define the segment's equation
    segmentInfiniteSlope = False
    segPnt1 = getCoordsIn2DBasis(coordinateSystem, segment[0])
    segPnt2 = getCoordsIn2DBasis(coordinateSystem, segment[1])
    linePnt = getCoordsIn2DBasis(coordinateSystem, linePointPosition)

    if segPnt1[0]== segPnt2[0]:
        # segment's equation: x = s
        segmentInfiniteSlope = True
        s = segPnt1[0]
    else:
        # segment's equation: y = s1 * x + s0
        s1 = (segPnt1[1] - segPnt2[1]) / (segPnt1[0] - segPnt2[0])
        s0 = segPnt1[1] - s1 * segPnt1[0]
     
    # Define the line's equation 
    lineInfiniteSlope = False
    if lineVector.dot(x) == 0.0:
        # line's equation: x = a
        lineInfiniteSlope = True
        a = linePnt[0]
    else:
        # line's equation: y = a1 * x + a0
        a1 = lineVector.dot(y) / lineVector.dot(x)
        a0 = linePnt[1] - a1 * linePnt[0]
         
    # Calculate the intersection
    if segmentInfiniteSlope == True and lineInfiniteSlope == True:
        return None
    elif segmentInfiniteSlope == True and lineInfiniteSlope == False:
        yI = a1 * s + a0
        roundedYI = yI
        if  roundedYI >= min(segPnt1[1], segPnt2[1]) and \
            roundedYI <= max(segPnt1[1], segPnt2[1]):
            return o + x * s + y * yI
        else:
            return None
    elif segmentInfiniteSlope == False and lineInfiniteSlope == True:
        yI = s1 * a + s0
        roundedYI = yI
        if  roundedYI >= min(segPnt1[1], segPnt2[1]) and \
            roundedYI <= max(segPnt1[1], segPnt2[1]):
            return o + x * a + y * yI
        else:
            return None
    else:
        xI = - (a0-s0) / (a1-s1)
        yI = a1 * xI + a0
        roundedYI = yI
        if  roundedYI >= min(segPnt1[1], segPnt2[1]) and \
            roundedYI <= max(segPnt1[1], segPnt2[1]):
            return o + x * xI + y * yI
        else:
            return None

lineIntersectSegment.numCalls = 0
lineIntersectSegment.failures = 0

def roundVec(vec3, decimals = approximation):
    rX = round(vec3.getX(), decimals)
    rY = round(vec3.getY(), decimals)
    rZ = round(vec3.getZ(), decimals)
    return Vec3(rX, rY, rZ)
    
def argMax(d):
    """
    Returns the key with the highest value.
    """
    if len(d.keys()) == 0: return None
    all = d.items()
    values = [x[1] for x in all]
    maxIndex = values.index(max(values))
    return all[maxIndex][0]

def argMin(d):
    """
    Returns the key with the lowest value.
    """
    if len(d.keys()) == 0: return None
    all = d.items()
    values = [x[1] for x in all]
    minIndex = values.index(min(values))
    return all[minIndex][0]

def convertVec3ToNp(vec):
    return np.ndarray((3,), buffer=np.array([vec.getX(), vec.getY(), vec.getZ()]), dtype=np.double)
#     return np.ndarray([vec.getX(), vec.getY(), vec.getZ()])

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    
    closed = set()
    fringe = Queue()
    state = problem.getStartState()
    node = [state, [], 0]
    fringe.push(node)
    while True:
        if fringe.isEmpty():
            return []
        node = fringe.pop()
        state = node[0]
        if problem.isGoalState(state):
            return node[1]
        elif state not in closed:
            closed.add(state)
            for child in problem.getSuccessors(state):
                childState = child[0]
                childAction = child[1]
                childNode = node[:]
                nodeAction = list(node[1])
                childNode[0] = childState
                childNode[1] = nodeAction
                childNode[1].append(childAction)
                childNode[2] += 1 # cost
                fringe.push(childNode)
     
def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    leaves = list()
    closed = set()
    fringe = PriorityQueue()
    node = problem.getStartNode()
    fringe.push(node, node[1])
    while True:
        if fringe.isEmpty():
            return leaves
        node = fringe.pop()
        state = node[0]
        if problem.isGoalState(state):
            return state
        elif state not in closed:
            closed.add(state)
            successors = problem.getSuccessors(state)
            if len(successors) > 0:
                for child in successors:
                    if child[0] not in closed:
                        fringe.push(child, child[1])
            else:
                leaves.append(node)

def perm(value, values):
    if value in values:
        return values[::-1][values.index(value)]
    else: return value
    
def profile(function_to_decorate):
    def wrapper(*args, **kwargs):
        pr = cProfile.Profile(timeunit=0.0000001)
        pr.enable()
        result = function_to_decorate(*args, **kwargs)
        pr.disable()
        s = StringIO.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print s.getvalue()
        exit()
        return result
    
    return wrapper

def calculateNewVelocity(velocity, slopeVec, cosTheta, avgLength, triNormal):
    # The correct way to calculate new velocity is to project the previous velocity onto the new slope
    # and use Bernulli's equation to calculate the new slope component 
    # The velocity will be higher for finer meshes, which is normal, since the calculation becomes more precise
#     velocity = velocity - velocity.project(triNormal)
    orthoVec = Vec3.cross(slopeVec, triNormal)
    oldSpeedSlopeComponent = Vec3.dot(slopeVec, velocity)
    oldSpeedOrthoComponent = Vec3.dot(orthoVec, velocity)
    try:
        newSpeed = slopeVec * math.sqrt( 2*GRAVITY*avgLength*cosTheta + oldSpeedSlopeComponent**2) + orthoVec * oldSpeedOrthoComponent
    except:
        print avgLength, cosTheta
    return newSpeed
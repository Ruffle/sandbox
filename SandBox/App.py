'''
Created on 14/02/2016

@author: Rafal Starzyk
'''

from direct.gui.DirectSlider import DirectSlider
from direct.gui.DirectCheckButton import DirectCheckButton
from direct.gui.DirectOptionMenu import DirectOptionMenu
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.ShowBase import ShowBase
from panda3d.core import TextNode, Vec3

from World import World

class App(ShowBase):
    def __init__(self):
        # Initialize the ShowBase class from which we inherit, which will
        # create a window and set up everything we need for rendering into it.
        ShowBase.__init__(self)
        self.world = None
        self.disableMouse()
#         self.camera.setPos(110, -90, 105)
        self.camera.setPos(50, 50, 200)
#         self.camera.setHpr(22.7, -37, 3.6)
        self.camera.setHpr(90,-90,0)
        self.setFrameRateMeter(True)
        self.setBackgroundColor(0,0,0)
        
        self.accept("p", self.pause)
        self.running = True
        self.wireframeOn()
        self._onscreenText = []
        
        self._currentIt = 0
        self._currentField = 0
        
#         self.pauseEventText = self.genLabelText("RUNNING", 0)       
        _wireframeCheckButton = self.genCheckButton("Wireframe", self.setWireframe, 0)
        _wireframeCheckButton["indicatorValue"] = True
        _wireframeCheckButton.setIndicatorValue()
        
        _stCheckButton = self.genCheckButton("Sediment transport", self.setSedimentTransport, 1)
        _stCheckButton["indicatorValue"] = True
        _stCheckButton.setIndicatorValue()
        
        self._dbCheckButton = self.genCheckButton("Drainage basins", self.setDrainageBasins, 2)
        self._dbCheckButton["indicatorValue"] = False
        self._dbCheckButton.setIndicatorValue()
        
        self._fieldMenu = DirectOptionMenu(text="field", scale=0.05,items=["elevation","velocity","flow"],initialitem=0,
                                      highlightColor=(0.65,0.65,0.65,1),command=self.setField)
        self._fieldMenu.setPos(Vec3(-1.25,0,-.95+.1*4))
        
        self._labelMenu = DirectOptionMenu(text="label", scale=0.05,items=["basinId","height","velocity","flow", "name"],initialitem=0,
                                      highlightColor=(0.65,0.65,0.65,1),command=self.setLabelType)
        self._labelMenu.setPos(Vec3(-1.25,0,-.95+.1*3))
        
        self._slider = {"value": 0}
    
    def genCheckButton(self, text, cmd, i):
        cbtn = DirectCheckButton(text = text ,scale=.05,command=cmd)
        cbtn.setPos(Vec3(-1.,0,-.95+.1*i))
        return cbtn
    
    def genLabelText(self, text, i):
        ost = OnscreenText(text = text, pos = (-1.3, .95-.05*i), fg=(1,1,1,1),
                           align = TextNode.ALeft, scale = .05, mayChange = 1)
        self._onscreenText.append(ost)
        return ost
        
    def clearText(self):
        for ost in self._onscreenText:
            ost.cleanup()
        self._onscreenText = []
        
    def setWireframe(self, boolean):
        if boolean:
            self.wireframeOn()
        else:
            self.wireframeOff()
    
    def setSedimentTransport(self, boolean):
        if self.world is not None:
            self.world.setShowSedimentTransport(boolean)
        
    def setDrainageBasins(self, boolean):
        if self.world is not None:
            self.world.setDrainageBasins(boolean)
        
    def setLabelType(self, type):
        print "type", type
        if self.world is not None:
            self.world.setLabelType(type)
        
    def setField(self, field):
        if self.world is not None:
            for section in self.world.sections:
                iteration = int(self._slider['value'])
                if field == "" and self._currentIt != iteration:
                    section.showVisualisation(iteration, self._fieldMenu.get())
                elif field != "" and self._currentField != field:
                    section.showVisualisation(iteration, self._fieldMenu.get())
                
                self._currentField = field
    
    def setIteration(self):
        self.setField("")
        self._currentIt = int(self._slider['value'])
    
    def pause(self):
#         print self.camera.getPos(), self.camera.getHpr()
        if self.running == True:
            print "Pausing Simulation"
#             self.pauseEventText.setText("PAUSED")
            self.enableMouse()
                                
        else:
            print "Resuming Simulation"
#             self.pauseEventText.setText("RUNNING")
            self.disableMouse()

        self.running = not self.running

    def createWorld(self):
        self.world = World(self)
    
    def addErosionSlider(self, numIt):
        self._slider = DirectSlider(range=(0,numIt), value=numIt, pageSize=3, command=self.setIteration)
        self._slider.setPos(Vec3(-1.,0,-.95+.1*5))
        self._slider.setScale(0.3)
        

'''
Created on 13/12/2016

@author: Rafael
'''
from collections import namedtuple
import math
from panda3d.core import Vec3
import time

from ErosionComputationGeometries import Triangle
from Flow import FluidTriFlow, FluidEdgeFlow
import Tools as T
import Drainage as D


class FlowCalculator(object):
    '''
    classdocs
    '''
    
    def __init__(self, endValue = -1, maxNumOccurences = float('inf')):
        '''
        Constructor
        '''
        self._branch = []# list holding the names of triangles in the current _branch
        self._divIdx = []# indices of triangles in _branch, where the flow splits
        self._endValue = endValue
        self._maxNumOccurences = maxNumOccurences
        
    def _endBranch(self):
#         print "branch end"
        if len(self._divIdx) == 0:
            self._branch = []
        else:
            self._branch = self._branch[:self._divIdx.pop()]
        
    def expand(self, flow):
        self._branch.append(flow.target.name)
        
        flow.onExpand()
        outflows = flow.calculateOutflows()
        length = len(outflows)
        
        if length > 0:
            if length > 1:
                # Branch out
                index = len(self._branch)
                self._divIdx.extend((length-1) * [index])
            
            for outflow in outflows:
                if self._branch.count(outflow.target.name) > self._maxNumOccurences or outflow.branchEndCondition() or outflow.value <= self._endValue:
                    # Branch end
                    outflow.onBranchEnd()
                    self._endBranch()
                else:
                    self.expand(outflow)
        else:
            # Branch end
            flow.onBranchEnd()
            self._endBranch()

Item = namedtuple("Item", ["tri", "outflows", "inflows", "avgLength"])

def _updateCoTris(delta, item, inflow, coItem, coInflow):
    item.inflows.remove(inflow)
    item.outflows[inflow] = delta
    for outflow in item.outflows:
        if outflow != inflow:
            item.outflows[outflow] = (1 - delta)/len(item.outflows)
    del coItem.outflows[coInflow]
    summ = sum(f for f in coItem.outflows.values())
    for outflow in coItem.outflows:
        coItem.outflows[outflow] /= summ

def _setFlowIO(tri, dir):
    if dir.length() > 0:
        domain = tri.getOutflowDomain(dir)
        outEdges = tri.domainToEdges[domain["index"]]
        i = domain["index"] % 3 # direction irrelevant
        i1 = T.perm(i, [1,2])
        i2 = T.perm(i, [0,1])
        i3 = T.perm(i, [0,2])
        
        point1 = tri.points[i1]
        point2 = tri.points[i2]
        
        alpha1 = domain["angle"]
        alpha2 = tri.angles[point2.name]
        alpha3 = math.pi - alpha1 - alpha2
        avgLength = 0.5 * tri.edges[i1].length * math.sin(alpha2) / math.sin(alpha3)
        
        newItem = Item(tri, {}, [], avgLength)
        if len(outEdges) == 1:
            outTri = outEdges[0].getNeighborTriangle(tri)
            
            if outTri is not None: newItem.outflows[outTri.name] = 1.0
        else:
            beta1 = tri.angles[point1.name] - alpha1
            beta3 = math.pi - alpha3
            
            out1 = 1 / (1 + (math.sin(beta1)*math.sin(beta3)*math.sin(alpha1+alpha3)) / (math.sin(alpha1)*math.sin(alpha3)*math.sin(beta1+beta3)) )
            out1 = max(0, min(1, out1))
            if 1 - out1 < 10e-3 or out1 < 10e-3:
                out1 = round(out1)
            
            assert out1 >= 0
            assert out1 <= 1
            out3 = 1.0 - out1
            
            tri1 = tri.edges[i1].getNeighborTriangle(tri)
            tri3 = tri.edges[i3].getNeighborTriangle(tri)
            
            if tri1 is not None and out1 > 0: newItem.outflows[tri1.name] = out1
            if tri3 is not None and out3 > 0: newItem.outflows[tri3.name] = out3
    else:
        newItem = Item(tri, {}, [], 0)
        for edge in tri.edges:
            neiTri = edge.getNeighborTriangle(tri)
            if neiTri is not None: newItem.outflows[neiTri.name] = 1.0/3.0
        
    if not tri.isBoundary(): assert len(newItem.outflows) > 0
    return newItem

def _enforceFlowConsistency(meshData, it):
    numRows = len(meshData["triangles"][it])
    
    # Calculate flows on triangles
    # Compute a map of triangle to outflow triangles with flow separation coefficients
    triToTrisMap = {}
    for iR in range(numRows):
        row = meshData["triangles"][it][iR]
        for tri in row:
            tri.order.setText("")
            tri.approxFlowDirection = Vec3(tri.approxFlowVelocity) if tri.approxFlowVelocity.length() > 0 else tri.slope
            tri.approxFlowDirection.normalize()
            newItem = _setFlowIO(tri, tri.approxFlowDirection)
            tri.flow = tri.getFlux()
            triToTrisMap[tri.name] = newItem

    # Add a list of inflow triangle names to the map
    for triName, item in triToTrisMap.iteritems():
#         print triName
        for outTriName, factor in item.outflows.iteritems():
#             print "\t", outTriName, factor
            triToTrisMap[outTriName].inflows.append(triName)
#         print
            
    # Resolve co-dependencies
    # 1. Resolve full codependencies
    recalclatedItems =  {}
    for triName, item in triToTrisMap.iteritems():
        hasCodependencies = len(item.inflows) > 0 and any(inflow in item.outflows for inflow in item.inflows)
        if hasCodependencies:
            coTris = filter(lambda inflow: inflow in item.outflows, item.inflows)
            coTris = map(lambda triName: triToTrisMap[triName].tri, coTris)
             
            # Correct directions
            newDirection = item.tri.approxFlowVelocity
            for tri in coTris:
                newDirection += tri.approxFlowVelocity*item.outflows[tri.name]
            newDirection.normalize()
             
            recalclatedItems[triName] = _setFlowIO(item.tri, newDirection)
             
    for triName, newItem in recalclatedItems.iteritems():
        oldItem = triToTrisMap[triName]
        inflows = filter(lambda inf: inf not in oldItem.outflows, oldItem.inflows)
        for a in inflows:
            newItem.inflows.append(a)
        for outflow in oldItem.outflows:
            if triName in triToTrisMap[outflow].inflows: 
                triToTrisMap[outflow].inflows.remove(triName)
        for outflow in newItem.outflows:
            neighborItem = recalclatedItems[outflow] if outflow in recalclatedItems else triToTrisMap[outflow]
            if triName not in neighborItem.inflows:
                neighborItem.inflows.append(triName)
        triToTrisMap[triName] = newItem
    
    # 2. Resolve partial codependencies
    remainingOutflows = {triName: len(item.outflows) for triName, item in triToTrisMap.iteritems()}
    resolvedDependencies =  {}
    for triName, item in triToTrisMap.iteritems():
        if "76" in triName: 
            pass
        hasCodependencies = len(item.inflows) > 0 and any(inflow in item.outflows for inflow in item.inflows)
        if hasCodependencies:
            coTris = filter(lambda inflow: inflow in item.outflows, item.inflows)
            if len(coTris) > 0:
#                 deltas = {}
                for inflow in item.inflows:
                    if inflow not in item.outflows: continue
                    coItem = triToTrisMap[inflow]
#                     deltas[inflow] = abs(item.outflows[inflow] - coItem.outflows[triName])
#                     inflow = T.argMin(deltas)
#                     coItem = triToTrisMap[inflow]
                    delta = item.outflows[inflow] - coItem.outflows[triName]
                     
                    if delta > 0:
                        if remainingOutflows[inflow] > 1:
                            remainingOutflows[inflow] -= 1
                            if triName not in resolvedDependencies:
                                resolvedDependencies[triName] = {}
                            resolvedDependencies[triName][inflow] = delta
                        else:
                            print remainingOutflows[inflow]
                            if inflow not in resolvedDependencies:
                                resolvedDependencies[inflow] = {}
                            resolvedDependencies[inflow][triName] = 1.0
                 
    for outName, outFlows in resolvedDependencies.iteritems():
        item = triToTrisMap[outName]
        for outflow, factor in item.outflows.iteritems():
            if outflow in outFlows:
                item.inflows.remove(outflow)
                item.outflows[outflow] = outFlows[outflow] #affect delta
                 
                coItem = triToTrisMap[outflow]
                del coItem.outflows[outName]
                if outflow not in resolvedDependencies:
                    summ = sum(f for f in coItem.outflows.values())
                    for coutflow in coItem.outflows:
                        coItem.outflows[coutflow] /= summ
#             else:
    for outName, outFlows in resolvedDependencies.iteritems():
        item = triToTrisMap[outName]
        for outflow, factor in item.outflows.iteritems():
            if outflow not in outFlows:
                sumUncorrected = 0
                for o in item.outflows:
                    if o not in outFlows:
                        sumUncorrected += item.outflows[o]
                item.outflows[outflow] = item.outflows[outflow] * (1 - sum(outFlows.values())) / sumUncorrected #normalize old factor

    # Filter out the triangles which are already flow consistent (0 inflows)
    stack = []
    for triName, item in triToTrisMap.iteritems():
        if len(item.outflows) == 0:
            print "lol"
        if len(item.outflows) == 1 and sum(item.outflows.values()) < 1 and not item.tri.isBoundary():
            pass
        if len(item.inflows) == 0:
            stack.insert(0, item)
            
    # and explore the triangles as a FIFO stack, eventually making them all flows consistent
    A = 0
    closedSet = set()
    while len(stack) > 0:
        top = stack.pop()
#             print top
        if isinstance(top, Item):
            if top.tri.name == "67-76-68":
                pass
            if top.tri.name in closedSet: continue
            closedSet.add(top.tri.name)
            A+=1
            item = top
            item.tri.flowVelocityDirection.normalize()
            flowVelocity = item.tri.flowVelocityDirection * item.tri.flowVelocityNorm
            newVelocity = T.calculateNewVelocity(flowVelocity / item.tri.flow, item.tri.slope, item.tri.cosTheta, item.avgLength, item.tri.normal)
#                 item.tri.avgFlowVelocity = newVelocity.project(item.tri.approxFlowDirection) DONT DO THISSSSS
            item.tri.avgFlowVelocity = newVelocity
#             item.tri.order.setText(str(A))
            for outTriName, factor in item.outflows.iteritems():
                if outTriName not in triToTrisMap: continue
                flow = item.tri.flow * factor
                outItem = triToTrisMap[outTriName]
                outItem.tri.flow += flow
                outItem.tri.flowVelocityNorm += item.tri.avgFlowVelocity.length() * flow
                outItem.tri.flowVelocityDirection += item.tri.avgFlowVelocity * flow
                if item.tri.name in outItem.inflows: outItem.inflows.remove(item.tri.name)
                
                if len(outItem.inflows) == 0:
                    stack.insert(0, outItem)
                else:
                    add = False
                    chain = []
                    while True:
                        if len(outItem.inflows) == 1:
                            outItem = triToTrisMap[outItem.inflows[0]]
                            if outItem in chain:
                                print "Cycle detected"
                                add = True
                                break
                            else:
                                chain.append(outItem)
                        else:
                            break
                    if add: 
                        stack.append(chain)
                    # Case where several triangles are co-dependent
#                     item1 = outItem
#                     item2 = triToTrisMap[item1.inflows[0]]
#                     chain = [item1, item2]
#                     add = True

#                         if len(item2.inflows) == 1:
#                             pass
#                         elif len(item2.inflows) == 2:
#                             while len(item2.inflows) == 2:
#                                 nextIdx = T.perm(item2.inflows.index(item1.tri.name), (0,1))
#                                 if item2.inflows[nextIdx] in item2.outflows:
#                                     item1 = item2
#                                     item2 = triToTrisMap[item1.inflows[nextIdx]]
#                                     chain.append(item2)
#                                 else:
#                                     # Check if the inflow leads back to a triangle in the chain
#                                     item3 = triToTrisMap[item1.inflows[0]]
#                                     while True:
#                                         if len(item3.inflows) == 1:
#                                             item3 = triToTrisMap[item3.inflows[0]]
#                                             if item3 in chain:
#                                                 break
#                                         else:
#                                             add = False
#                                             break
#                                     break
#                         elif len(item2.inflows) == 3:
#                             add = False
                        
#                     if add: 
#                         stack.append(chain)
        else:
            # Case where two triangles are co-dependent
            items = top
            coOutFlows = {}
            itemNames = map(lambda i: i.tri.name, items)
            for i, item in enumerate(items):
                outflowsToDelete = []
                for outflowTriName, factor in item.outflows.iteritems():
                    if outflowTriName not in itemNames: continue
                    
                    outflowsToDelete.append(outflowTriName)
                    outItem = triToTrisMap[outflowTriName]
                    coOutFlows[outflowTriName] = {}
                    coOutFlows[outflowTriName]["flow"] = item.tri.flow * factor
#                     newVelocity = T.calculateNewVelocity(item.tri.flowVelocity / item.tri.flow, item.tri.slope, item.tri.cosTheta, item.avgLength, item.tri.normal)
                    flowDirection = Vec3(item.tri.flowVelocityDirection)
                    flowDirection.normalize()
                    flowVelocity = flowDirection * item.tri.flowVelocityNorm / item.tri.flow
                    coOutFlows[outflowTriName]["flowVelocityNorm"] = flowVelocity.length() * coOutFlows[outItem.tri.name]["flow"]
                    coOutFlows[outflowTriName]["flowVelocityDirection"] = flowVelocity * coOutFlows[outItem.tri.name]["flow"]
                    if item.tri.name in outItem.inflows: 
                        outItem.inflows.remove(item.tri.name)
                    
                for outflowTriName in outflowsToDelete:#filter(lambda n: n in itemNames, item.outflows.keys()): 
                    del item.outflows[outflowTriName]
            
            for item in items:
                if len(item.inflows) == 0:
                    stack.insert(0, item)
                if item.tri.name in coOutFlows:
                    item.tri.flow += coOutFlows[item.tri.name]["flow"]
                    item.tri.flowVelocityNorm += coOutFlows[item.tri.name]["flowVelocityNorm"]
                    item.tri.flowVelocityDirection += coOutFlows[item.tri.name]["flowVelocityDirection"]
            
    print A, numRows * len(meshData["triangles"][it][0])
#     assert A == numRows * len(meshData["triangles"][it][0]), "{0} != {1}".format(A, numRows * len(meshData["triangles"][it][0]))

def calculateFluidFlows(meshData, it, lastIteration):
        # For each triangle, calculate how the flux divides
        # and pours onto the neighboring triangles
        flowStartTime = time.time()
        
        # Start by wiping the data of the previous iteration if any
        for row in meshData["points"]:
            for point in row:
                point.avgFlowVelocity = Vec3(0,0,0)
                point.flow = 0
        for kind,edgeGrid in meshData["edges"][it].iteritems():
            for edgeRow in edgeGrid:
                for edge in edgeRow:
                    edge.flow = 0.0
                    edge.avgFlowVelocity = Vec3(0,0,0)
        for row in meshData["triangles"][it]:
            for triangle in row:
                triangle.flow = 0.0
                triangle.flowVelocityNorm = 0
                triangle.flowVelocityDirection = Vec3(0,0,0)
                triangle.approxFlowDirection = Vec3(0,0,0)
                triangle.approxFlowVelocity = Vec3(0,0,0)
                triangle.avgFlowVelocity = Vec3(0,0,0)
                #triangle.order.setText("")
             
        numRows = len(meshData["triangles"][it])
        
        # As the fluid flows down the mesh, the triangles it passes through form a tree structure
        iTri = 0
        maxOccurences = 1
        numTris = numRows * len(meshData["triangles"][it][0])
        for iR in range(numRows):
            row = meshData["triangles"][it][iR]
            for tri in row:
                flux = tri.getFlux()
                fc = FlowCalculator(flux/5, maxNumOccurences=maxOccurences)
                flow = FluidTriFlow(str(iTri), flux, None, tri)
                fc.expand(flow)
                iTri += 1
                
                if maxOccurences > 2:
                    # Erase flownames to increase performance
                    for rowrow in meshData["triangles"][it]:
                        for triangle in rowrow:
                            triangle.flowNames = []
                        
        # Calculate flows coming from lake heads
        for basinId, head in D.basinHeads.iteritems():
            if head["flow"] > 0:
                velocity = head["velocity"] / head["flow"]
                points = D.basinHeads[basinId]["points"]
                lenHd = len(points)
                for point in points:
                    outflowEdges = filter(lambda e: e.getOtherPoint(point.name).drainageBasinId != basinId, point.edges)
                    lenEG = len(outflowEdges)
                    for outflowEdge in outflowEdges:
                        flow = FluidEdgeFlow(head["flow"]/(lenHd*lenEG), point, outflowEdge, outflowEdge.slope * velocity)
                        calc = FlowCalculator(maxNumOccurences = 5)
                        calc.expand(flow)
        
        _enforceFlowConsistency(meshData, it)
        
        # Calculate average flow velocities on triangles
        for iR in range(numRows):
            row = meshData["triangles"][it][iR]
            for tri in row:
                tri.flows.append(tri.flow)
#                 tri.avgFlowVelocity = tri.approxFlowVelocity/tri.flow
                tri.avgFlowVelocities.append(tri.avgFlowVelocity.length())
                if lastIteration:
                    tri.avgFlowVelocities.append(0)
                    tri.flows.append(0)
        
        # Calculate flows on edges
        for kind,edgeGrid in meshData["edges"][it].iteritems():
            for edgeRow in edgeGrid:
                for edge in edgeRow:
                    if edge.flow > 0:
                        edge.avgFlowVelocity = edge.avgFlowVelocity/edge.flow
                    
#                     triFlow = 0
#                     triAvgFlowVelocity = Vec3()
#                     for tri in edge.triangles:
#                         triFlow += tri.flow
#                         triAvgFlowVelocity += tri.avgFlowVelocity
#                     triFlow /= len(edge.triangles)
#                     triAvgFlowVelocity /= len(edge.triangles)
#                     
#                     edge.flow += triFlow
#                     edge.avgFlowVelocity += triAvgFlowVelocity
                    
                    edge.updateFlowDirection(edge.avgFlowVelocity)
                    if edge.flowDirection == "":
                        edge.updateFlowDirection(edge.slope) # useless
        
        # Calculate average flows on points
        # 1. Start with 8 edge points
        for row in meshData["points"]:
            for point in row:
                # Get edge flows
                edgeVelocity = Vec3()
                edgeVelocityNorm = 0
                edgeVelocityDirection = Vec3()
                edgeFlow = 0
                numEdges = len(point.edges)
                for edge in point.edges:
                    edgeVelocityNorm += edge.avgFlowVelocity.length() * edge.flow
                    edgeVelocityDirection += edge.avgFlowVelocity * edge.flow
                    edgeFlow += edge.flow
                if edgeFlow > 0: edgeVelocityNorm /= edgeFlow
                edgeVelocityDirection.normalize()
                edgeVelocity = edgeVelocityDirection * edgeVelocityNorm
                edgeFlow /= numEdges
                
                # Get tri flows
                pointTriangles = point.getNeighborTriangles()
                numTris = len(pointTriangles)
                triVelocity = Vec3()
                triVelocityNorm = 0
                triVelocityDirection = Vec3()
                triFlow = 0
                for tri in pointTriangles:
                    triVelocityNorm += tri.avgFlowVelocity.length() * tri.flow
                    triVelocityDirection += tri.avgFlowVelocity * tri.flow
                    triFlow += tri.flow
                    
                    if numEdges == 4:
                        opEdge = tri.getOppositeEdge(point)
                        nTri = opEdge.getNeighborTriangle(tri)
                        triVelocityNorm += nTri.avgFlowVelocity.length() * nTri.flow
                        triVelocityDirection += nTri.avgFlowVelocity * nTri.flow
                        triFlow += nTri.flow
                        numTris += 1
                
                if triFlow > 0: triVelocityNorm /= triFlow
                triVelocityDirection.normalize()
                triVelocity = triVelocityDirection * triVelocityNorm
                triFlow /= numTris
                
                point.flow = edgeFlow + triFlow
                totalVelocity = (edgeVelocity.length() * edgeFlow + triVelocity.length() * triFlow) / point.flow
                totalDirection = edgeVelocity * edgeFlow + triVelocity * triFlow
                totalDirection.normalize()
                point.avgFlowVelocity = totalDirection * totalVelocity
                
                point.flows.append(point.flow)
                point.avgFlowVelocities.append(point.avgFlowVelocity.length())
                
#                 isEven = not isEven
                
        # 2. For 4 edge points calculate average from 8 edge points
#         isEven = True
#         for row in meshData["points"]:
#             for point in row:
#                 if not isEven:
#                     for edge in point.edges:
#                         otherPoint = edge.getOtherPoint(point.name)
#                         point.flow += otherPoint.flow
#                         point.avgFlowVelocity += otherPoint.avgFlowVelocity
#                     point.flow /= len(point.edges)
#                     point.avgFlowVelocity /= len(point.edges)
#                 isEven = not isEven
        
        print "Calculated flows in", time.time() - flowStartTime, "seconds."
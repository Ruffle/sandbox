'''
Created on 13/12/2016

@author: Rafael
'''
import math
from panda3d.core import Vec3

import ErosionComputationGeometries as ECG
from Globals import *
from Tools import roundVec, drainage
import Tools as T
import Drainage as D
import Intersector as I
from Tests import avgLen

class BaseFlow(object):
    '''
    classdocs
    '''

    def __init__(self, value, source, target):
        '''
        Constructor
        '''
        self.value = value
        self.source = source
        self.target = target
        
    def __eq__(self, otherFlow):
        if  self.value  == otherFlow.value and \
            self.source == otherFlow.source and \
            self.target == otherFlow.target:
            return True
        else:
            return False
        
    def onBranchEnd(self):
        pass
        
    def branchEndCondition(self):
        return False

class TriFlow(BaseFlow):
    def createLocalCoordinateSystem(self, triangle):
        # Create a 2D local coords system whoose x axis runs along the source edge and the y axis points inside the triangle
        origin = self.source.points[0].position
        x = self.source.points[1].position - self.source.points[0].position
        x.normalize()
        y = triangle.normal.cross(x)
        y.normalize()
        inwardsPointingVector = triangle.getOppositePoint(self.source).position - self.source.points[0].position
        if y.dot(inwardsPointingVector) < 0: # left-handed
            origin = self.source.points[1].position
            x = -x
            y = -y
        return {"origin": origin, "x": x, "y": y}

class FluidTriFlow(TriFlow): # degeneration into edge flow is OFF
    '''
    classdocs
    '''

    def __init__(self, name, value = 0.0, sourceEdge = None, target = None, speed = Vec3(), sourceEdgePositions = []):
        '''
        Constructor
        '''
        super(FluidTriFlow, self).__init__(value, sourceEdge, target)
        self.name = name
        self.speed = speed
        self.sourceEdgePositions = sourceEdgePositions
        
    def __repr__(self):
        return "{}(value = {}, sourceEdge = {}, target = {}, speed = {}, sourceEdgePositions = {}".format(
            self.__class__, self.value, self.source, self.target, self.speed, self.sourceEdgePositions
        )
        
    def __eq__(self, otherFlow):
        if not super(FluidTriFlow, self).__eq__(otherFlow):
            return False
        else:
            if  self.speed.almostEqual(otherFlow.speed, 10e-5) and \
                self.sourceEdgePositions == otherFlow.sourceEdgePositions:
                return True
            else:
                return False
        
    def __add__(self, otherFlow):
        assert isinstance(otherFlow, FluidTriFlow), "Invalid addend type: " + str(type(otherFlow)) + ". Expected FluidTriFlow."
        assert self.source is otherFlow.source, "Flows with different source edges cannot be added: " + str(self.source) + " != " +  str(otherFlow.source)
        
        # Sum the edge positions by taking their min and max
        totalValue = self.value + otherFlow.value
        assert totalValue >= 0.0
        weighedSpeed = (self.speed*self.value + otherFlow.speed*otherFlow.value) / totalValue
        
        edgeVector = self.source.points[0].position - self.source.points[1].position
        edgeVector.normalize()
        edgePositions = self.sourceEdgePositions + otherFlow.sourceEdgePositions
        if len(edgePositions) > 0:
#             edgePosProjected = map(lambda pos: (edgeVector.dot( pos - self.source.points[1].position ), pos), edgePositions)
#             edgePosProjected.sort()
#             
#             assert not edgePosProjected[0][1].almostEqual( edgePosProjected[-1][1], 10e-5)
#             
#             return FluidTriFlow(totalValue, self.source, self.target, weighedSpeed, [edgePosProjected[0][1], edgePosProjected[-1][1]])
            return FluidTriFlow(self.name + '+' + otherFlow.name, totalValue, self.source, self.target, weighedSpeed, [min(edgePositions), max(edgePositions)])
        else:
            return FluidTriFlow(self.name + '+' + otherFlow.name, totalValue, self.source, self.target, weighedSpeed, [])
        
    def __radd__(self, otherFlow):
        if otherFlow == 0: # In sum
            return self
        else:
            return self.__add__(otherFlow) # addition is commutative
    
    def onExpand(self):
#         if not self.target.hasFlow(self.name):
        self.target.flow += self.value
        self.target.approxFlowVelocity += self.speed * self.value
#             self.target.flowNames.append(self.name)
            
#         if any(map(lambda c: math.isinf(c), [self.target.approxFlowVelocity.getX(), self.target.approxFlowVelocity.getY(), self.target.approxFlowVelocity.getZ()])):
#             raise Exception("Average fluid speed infinite")

    def onBranchEnd(self):
        # Check if the flow goes into a lake
        if self.target.isInLake(True):
#                 return
            basinId = filter(lambda p: p.isInLake(), self.target.points)[0].drainageBasinId
            D.basinHeads[basinId]["flow"] += self.value
            D.basinHeads[basinId]["velocity"] += self.speed.length() * self.value
        
    def calculateMaxSpeed(self, cosTheta):
#         flowHeight = 1
#         MASS_PER_VOLUME_FLUID * GRAVITY * cosTheta * flowHeight**2 / (3 * VISCOSITY)
        return MASS_PER_VOLUME_FLUID * GRAVITY * cosTheta / (3 * VISCOSITY * 10**5)
    
    @staticmethod
    def correctFlows(triangle, outFlows, correctedOutFlows, sourceEdge, type): # Not compatible with triangle.hasFlow
        FluidTriFlow.correctFlows.numCalls += 1
        if FluidTriFlow.correctFlows.numCalls > 3:
            # There's no reasonable scenario where so many recalculations would be needed
            return
        for outFlow in outFlows:
            # Check if the flow's trajectory was modified to the point where it doesn't exit through the same edge
            newDomain = triangle.getOutflowDomain(outFlow.speed)
            
            newOutFlowEdges = triangle.domainToEdges[newDomain["index"]]
            
            if outFlow.source not in newOutFlowEdges:
                # Recalculate a "corrected" outflow, thus giving a better approximation of the physical phenomenon
                recalculatedOutflows = outFlow.calculateOutflowsFromIntersections2(triangle) if type == 2 else outFlow.calculateOutflowsFromIntersections1(triangle)
                FluidTriFlow.correctFlows(triangle, recalculatedOutflows, correctedOutFlows, sourceEdge, type)
            else:
                correctedOutFlows.append(outFlow)
    
    def calculateOutflowsFromIntersections2(self, triangle):
        if self.sourceEdgePositions == []:
            self.sourceEdgePositions = [0,1]
        
        outflows = []
        
        direction = self.speed if self.speed.length() > 0 else triangle.slope
        if direction.length() > 0:
            domain = triangle.getOutflowDomain(direction)
            outFlowEdges = triangle.domainToEdges[domain["index"]]
        else:
            outFlowEdges = triangle.edges
        
        numOutflowEdges = len(outFlowEdges)
        if numOutflowEdges == 1 or numOutflowEdges == 2:
            
            its = I.Intersector(triangle, domain, self.source)
            its.calculateIntersections(*self.sourceEdgePositions)
            
            for iOE, outflowEdge in enumerate(outFlowEdges):
                if not outflowEdge.name in its.intersections: continue
                outflowTargetTriangle = outflowEdge.getNeighborTriangle(triangle)
                if outflowTargetTriangle is None: continue
                
                edgeIntersections = its.intersections[outflowEdge.name]
                numInt = len(edgeIntersections)
                intLengths = map(lambda i: i["length"], edgeIntersections)
                oeps = map(lambda i: i["outPosition"], edgeIntersections)
                valFactor = 1.
                if numInt == 1: #Case of flow exiting via 2 edges
                    intLengths.append(its.maxIntLength)
                    oeps.append(0 if edgeIntersections[0]["edgeIdx"] == 1 else 1)
                    
                    valFactor = (abs(its.lengthProp2 - edgeIntersections[0]["inPosition"]) / 
                        abs(self.sourceEdgePositions[0] - self.sourceEdgePositions[1]))
#                 outflowExtent = abs(oeps[0] - oeps[1])
                
                avgLength = sum(intLengths) / 2.
                newSpeed = T.calculateNewVelocity(self.speed, triangle.slope, triangle.cosTheta, avgLength, triangle.normal)
                
                # Check orientation in target triangle, flip positions if necessary
                if outflowTargetTriangle.getEdgeOrientation(outflowEdge.name) != triangle.getEdgeOrientation(outflowEdge.name):
                    oeps = map(lambda p: 1-p, oeps)
                
                outflowName = self.name if numOutflowEdges == 1 else self.name + ':' + str(iOE)
                outflows.append( FluidTriFlow(outflowName, self.value*valFactor, outflowEdge, outflowTargetTriangle, newSpeed, oeps) )
                
        elif numOutflowEdges == 3:
            for iOE, outflowEdge in enumerate(outFlowEdges):
                outflowTargetTriangle = outflowEdge.getNeighborTriangle(triangle)
                outflowName = self.name + ':' + str(iOE)
                outflows.append( FluidTriFlow(outflowName, self.value/3.0, outflowEdge, outflowTargetTriangle, Vec3(0,0,0), [0,1]) )
        else:
            raise Exception("Wrong number of outflow edges")
        
        return tuple(outflows)
        
    def calculateOutflowsFromIntersections1(self, triangle):
        if self.sourceEdgePositions == []:
            # One outflow edge and one intersection
            self.sourceEdgePositions = [point.position for point in self.source.points]
        
        coordsSystem = self.createLocalCoordinateSystem(triangle)
        
        # Calculate source edge incoming flow positions in the local coordinate system
        # and arrange them by increasing x value
        sourceEdgePositionsLocal = map(lambda pos: Vec3.length(pos - coordsSystem["origin"]), self.sourceEdgePositions)
        sourceEdgePositionsLocal.sort()
        self.sourceEdgePositions.sort(key=lambda pos: Vec3.length(pos - coordsSystem["origin"]))
#         assert not self.sourceEdgePositions[0].almostEqual(self.sourceEdgePositions[1], 10e-5), "Source edge positions too close"
        
        # Calculate self's projected extent on the outflow edges
        projections = []
        outFlowEdges = filter(lambda edge: edge is not self.source, triangle.edges)

        for ofe in outFlowEdges:
            projection = {"edge": ofe, "flowLines": []}
            outFlowEdgeEnds = [T.roundVec(point.position) for point in ofe.points]
            for sep in self.sourceEdgePositions:
                roundedSep = T.roundVec(sep)
                if roundedSep in outFlowEdgeEnds:
                    # No need to calculate an intersection
                    projection["flowLines"].append( (sep, sep) )
                else:
                    direction = self.speed if self.speed.length() > 0 else triangle.slope
                    try:
                        intersectionPoint = T.lineIntersectSegment(coordsSystem, sep, direction, outFlowEdgeEnds)
                        if intersectionPoint != None:
                            projection["flowLines"].append( (sep, intersectionPoint) )
                    except:
                        T.lineIntersectSegment.failures += 1
                        
            projections.append(projection)
        
        # Calculate outflows
        outflows = []
        if len(projections) > 0:
            for proj in projections:
                if len(proj["flowLines"]) == 2:
                    # Flow exits through one edge
                    projLengths = []
                    for edgeCouple in proj["flowLines"]:
                        projLengths.append( (edgeCouple[0] - edgeCouple[1]).length() )
                    avgLength = sum(projLengths) / 2.0
                        
                    newSpeed = T.calculateNewVelocity(self.speed, triangle.slope, triangle.cosTheta, avgLength)
                    
                    if not proj["flowLines"][0][1].almostEqual( proj["flowLines"][1][1], 10e-5 ):
                        newSourceEdgePositions = [edgeCouple[1] for edgeCouple in proj["flowLines"]]
#                         for pos in newSourceEdgePositions:
#                             assert pos in proj["edge"]
                        outflows = ( FluidTriFlow(self.value, proj["edge"], None, newSpeed, newSourceEdgePositions), )
                    # else flow became too narrow and ended
                    return tuple(outflows)

            try:
                # Flow exits through two edges
                commonPoint = ECG.Edge.getCommonPoint(outFlowEdges[0], outFlowEdges[1])
                direction = self.speed if self.speed.length() > 0 else triangle.slope
                projectionOnSourceEdge = T.lineIntersectSegment(coordsSystem, commonPoint.position, direction, self.sourceEdgePositions)
                if projectionOnSourceEdge is None:
                    #Flow became too narrow and ended
                    return tuple(outflows)
                longestPath = (projectionOnSourceEdge - commonPoint.position).length()
                projectionLength = (projectionOnSourceEdge - coordsSystem["origin"]).length()
                deltaX = abs(sourceEdgePositionsLocal[1] - sourceEdgePositionsLocal[0])
    
                for proj in projections:
                    flowLine = proj["flowLines"][0]
                    sep = flowLine[0]
                    avgLength = (longestPath + (flowLine[0] - flowLine[1]).length()) / 2.0
                    newSpeed = T.calculateNewVelocity(self.speed, triangle.slope, triangle.cosTheta, avgLength)
                    sepIndex = self.sourceEdgePositions.index(sep)
                    sepLocalX = sourceEdgePositionsLocal[sepIndex]
                    factor = abs(sepLocalX - projectionLength) / deltaX
                    if not flowLine[1].almostEqual( commonPoint.position, 10e-5 ):
                        outflows.append( FluidTriFlow(factor*self.value, proj["edge"], None, newSpeed, [flowLine[1], commonPoint.position]) )
                    # else flow became too narrow and ended
            except Exception as e:
                T.lineIntersectSegment.failures += 1
                
        return tuple(outflows)
    
    def calculateOutflows(self, testing = False, type = 2):
        # sourceEdge = None means that triangle is the source triangle
        # sourceEdgePositions = [] means that the self goes through the whole edge
#         if len(self.sourceEdgePositions) == 2:
#             assert not self.sourceEdgePositions[0].almostEqual(self.sourceEdgePositions[1], 10e-5)
        triangle = self.target # for clarity
        outflows = []
        
        if not triangle.isBedrock():#not triangle.isInLake() and 
            if self.source == None:
                if self.speed.length() == 0 and triangle.slope.length() == 0:
                    # Case of a flat triangle, equal outflows in every direction
                    centerPos = triangle.getCenter()
                    
                    for i in range(3):
                        vec = centerPos - triangle.points[i].position
                        edgeVec = triangle.directedEdges[i]["vector"] * triangle.edges[i].length
                        area = Vec3.length(vec.cross(edgeVec) * 0.5)
                        
                        edge = triangle.directedEdges[i]["edge"]
                        outValue = area / triangle.area * self.value
                        outflows.append( FluidTriFlow(self.name + ':' + str(i), outValue, edge, None, Vec3(0,0,0), [0,1]) ) # speed cosidered negligeable
                
                else:
                    domain = triangle.getOutflowDomain(triangle.slope)
                    outFlowEdges = triangle.domainToEdges[domain["index"]]
                    
                    if len(outFlowEdges) == 2:
                        for edge in triangle.edges:
                            if edge not in outFlowEdges:
                                self.source = edge
                        assert self.source is not None
                        outflows = self.calculateOutflowsFromIntersections2(triangle) if type == 2 else self.calculateOutflowsFromIntersections1(triangle)
                    else:
                        direction = self.speed if self.speed.length() > 0 else triangle.slope
                        domain = triangle.getOutflowDomain(direction)
                        its = I.Intersector(triangle, domain, self.source)
    #                     avgLength = triangle.calculateAverageFlowLength()
                        avgLength = its.maxIntLength/2.
                        newSpeed = T.calculateNewVelocity(self.speed, triangle.slope, triangle.cosTheta, avgLength, triangle.normal)
                        outflows = ( FluidTriFlow(self.name, self.value, outFlowEdges[0], None, newSpeed), )
            
            # Check if the flow goes into a lake
            else:
                outflows = self.calculateOutflowsFromIntersections2(triangle) if type == 2 else self.calculateOutflowsFromIntersections1(triangle)
                
                correctedOutFlows = outflows
    #             correctedOutFlows = []
    #             FluidTriFlow.correctFlows.numCalls = 0
    #             FluidTriFlow.correctFlows(triangle, outflows, correctedOutFlows, self.source, type)
    #             correctedOutFlows = tuple(correctedOutFlows)
                
                if outflows == correctedOutFlows:
                    outflows = correctedOutFlows
                else:
                    # Sum the corrected outflows
                    summedFlows = []
                    if len(correctedOutFlows) > 0:
                        flowsByEdge = {}
                        for correctedOutFlow in correctedOutFlows:
                            if correctedOutFlow.source.name not in flowsByEdge:
                                flowsByEdge[correctedOutFlow.source.name] = []
                            flowsByEdge[correctedOutFlow.source.name].append(correctedOutFlow)
                            
                        for edgeName, flows in flowsByEdge.iteritems():
                            summedFlows.append( sum(flows) )
                    # else all the flows ended during correction
                    
                    outflows = summedFlows
                    
        # For each outflow add a target triangle
        for outflow in outflows:
            outflow.target = outflow.source.getNeighborTriangle(triangle)
        
        if testing == False:
            # Filter flows that are going nowhere
            outflows = filter(lambda outflow: outflow.target != None, outflows)
        return tuple(outflows)

class FluidEdgeFlow(BaseFlow):
    '''
    classdocs
    '''
    
    def __init__(self, value = 0.0, source = None, target = None, speed = Vec3()):
        '''
        Constructor
        '''
        super(FluidEdgeFlow, self).__init__(value, source, target)
        self.speed = speed
        
    def __eq__(self, otherFlow):
        if not super(FluidEdgeFlow, self).__eq__(otherFlow):
            return False
        else:
            if  self.speed.almostEqual(otherFlow.speed, 10e-5):
                return True
            else:
                return False
        
    def onExpand(self):
        if self.source is not None:
            self.target.flow += self.value
            self.target.avgFlowVelocity += self.speed * self.value

#             if any(map(lambda c: math.isinf(c), [self.target.approxFlowVelocity.getX(), self.target.approxFlowVelocity.getY(), self.target.approxFlowVelocity.getZ()])):
#                 raise Exception("Average fluid speed infinite")

    def onBranchEnd(self):
        return
        pass
        if self.source is not None:
            # Check if the flow goes into a lake
            if self.target.isInLake():
                basinId = self.source.drainageBasinId
                try:
                    FluidEdgeFlow.transferToHeads(basinId, self.value)
                except:
                    pass
                    print "get fukd1"
    
    def calculateNewSpeed(self):
        orthoVec = Vec3.cross(self.target.slope, self.target.getNormal())
        oldSpeedSlopeComponent = Vec3.dot(self.target.slope, self.speed)
        oldSpeedOrthoComponent = Vec3.dot(orthoVec, self.speed)
        newSpeed = self.target.slope * math.sqrt( 2*GRAVITY*self.target.length*self.target.cosTheta + oldSpeedSlopeComponent**2) + orthoVec * oldSpeedOrthoComponent
#         newSpeed = self.target.slope * 2*math.sqrt( GRAVITY**self.target.cosTheta) + self.speed
        
#         if any(map(lambda c: math.isinf(c), [newSpeed.getX(), newSpeed.getY(), newSpeed.getZ()])):
#             raise Exception("Fluid speed infinite")
        
        return newSpeed
    
    def calculateOutflows(self):
        outflows = []
        
        # Find target point
        edgeVec = self.target.points[0].position - self.target.points[1].position
        dot = edgeVec.dot(self.speed)
        if dot > 0:
            targetPoint = self.target.points[0]
        else:
            targetPoint = self.target.points[1]
        
        # Check if point is inside a lake
        if self.source is None or not self.source.isBedrock():#not self.source.isInLake() and
            newSpeed = self.calculateNewSpeed()
            newSpeedLength = newSpeed.length()
            normalizedDir = Vec3(newSpeed)
            normalizedDir.normalize()
            
            outflowEdges = (edge for edge in targetPoint.edges if edge != self.target)
            
            sumDots = 0.
            factors = []
            directions = []
            finalOutflowEdges = []
            for outflowEdge in outflowEdges:
                if not outflowEdge.isConcave:
                    continue
#                 if outflowEdge.isInLake():
#                     continue
                dir  = outflowEdge.getVectorStartingAt(targetPoint)
                sdot = normalizedDir.dot(dir)
                if sdot <= 0:
                    continue
                gdot = ug.dot(dir)
                if gdot <= 0:
                    continue
                factors.append( sdot )
                directions.append( dir )
                sumDots += sdot
                finalOutflowEdges.append(outflowEdge)
            
#             for i, finalOutflowEdge in enumerate(finalOutflowEdges):
#                 factor = factors[i] / sumDots
#                 outflows.append( FluidEdgeFlow(factor * self.value, targetPoint, finalOutflowEdge, directions[i]*factors[i]*newSpeedLength) )
                 
            if len(finalOutflowEdges) > 0:
                maxDot = max(factors)
                maxIdx = factors.index(maxDot)
                outflows = (FluidEdgeFlow(self.value, targetPoint, finalOutflowEdges[maxIdx], directions[maxIdx]*maxDot*newSpeedLength), )
             
        return tuple(outflows)
    

class SedimentTriFlow(TriFlow):
    '''
    classdocs
    '''
    
    def __init__(self, value = 0.0, sourceEdge = None, target = None, avgGrainSize = AVG_GRAIN_SIZE):
        '''
        Constructor
        '''
        super(SedimentTriFlow, self).__init__(value, sourceEdge, target)
        self.initialValue = value
        self.avgGrainSize = avgGrainSize
        self.hasDeposited = False  # if anything has been deposited yet
        
    def _deposit(self, obj, depth, numPoints):
#         unitVolumeIncrease = T.calculateTransportedVolume(triangle, 1)
#         depth = volumeToDeposit / unitVolumeIncrease
#         depth = T.limitSedimentDepth(triangle, depth, "max")
#         volumeToDeposit = depth * unitVolumeIncrease
        self.value -= depth
#         depositionVec = Vec3.unitZ() * depth / numPoints
        
        # Register point position delta after erosion
        for point in obj.points:
            point.delta += depth / numPoints
#             point.deltaPosition += depositionVec
            
        self.hasDeposited = True
        
    def _depositOnTriangle(self, triangle, depth):#volumeToDeposit):
        self._deposit(triangle, depth, 3.0)
        
    def _depositOnEdge(self, edge, depth):#volumeToDeposit):
        self._deposit(edge, depth, 2.0)
        
    def onExpand(self):
        pass
    
    def onBranchEnd(self):
        self.avgGrainSize = 0.0
        if self.source is not None:
            self._depositOnEdge(self.source, self.value)
        
    def branchEndCondition(self):
        return self.source.approxFlowVelocity.length() < Vd0
    
    def calculateOutflows(self):
        triangle = self.target # for clarity
        speedVec = triangle.approxFlowVelocity
        domain = triangle.getOutflowDomain(speedVec)
        outFlowEdges = triangle.domainToEdges[domain["index"]]
        
        # Check if the sediment flows back onto the previous triangle
        for outFlowEdge in outFlowEdges:
            if outFlowEdge is self.source:
                # Deposit everything
                volumeToDeposit = self.value
                self.avgGrainSize = 0.0
                self._depositOnEdge(self.source, volumeToDeposit)
                return tuple()
            
        outflows = []
        if len(outFlowEdges) == 1:
            outflows.append( SedimentTriFlow(self.value, outFlowEdges[0]), )
        # Set the source edge
        else:
            for edge in triangle.edges:
                if edge not in outFlowEdges:
                    self.source = edge
                    
            coordsSystem = self.createLocalCoordinateSystem(triangle)
            
            commonPoint = ECG.Edge.getCommonPoint(outFlowEdges[0], outFlowEdges[1])
            
            # Calculate projection of the common point
            segment = [point.position for point in self.source.points]
            try:
                projectionOnSourceEdge = T.lineIntersectSegment(coordsSystem, commonPoint.position, speedVec, segment)
                assert projectionOnSourceEdge is not None, "Couldn't project common point in two-edge outflow. Check segment width"
            except Exception:
                T.lineIntersectSegment.failures += 1
                return ()
            # Determine how much sediment goes with each outflow
            for point in self.source.points:
                for outFlowEdge in outFlowEdges:
                    if point in outFlowEdge.points:
                        factor = (point.position - projectionOnSourceEdge).length() / self.source.length
                        outflows.append( SedimentTriFlow(factor * self.value, outFlowEdge) )
            assert len(outflows) == 2
            
        # For each outflow add a target triangle
        for outflow in outflows:
            outflow.target = outflow.source.getNeighborTriangle(triangle)
        
        # Filter flows that are going nowhere
        outflows = filter(lambda outflow: outflow.target != None, outflows)
        return tuple(outflows)

class SedimentEdgeFlow(BaseFlow):
    '''
    classdocs
    '''
    
    def __init__(self, value = 0.0, source = None, target = None):
        '''
        Constructor
        '''
        super(SedimentEdgeFlow, self).__init__(value, source, target)
        
    def _deposit(self, point, depth):
        if point.position.getX() == 50 and point.position.getY() == 50:
            pass
#         depth = point.getAdmissibleDeltaHeight(depth)
        self.value -= depth
        
        # Register point position delta after erosion
        point.delta += depth
#         print "deposited "+str(depth)+ " on "+point.name
    
    def onExpand(self):
        # Get the point's sediment if it hasn't eroded yet
        if not self.source.hasEroded and self.source.delta < 0: 
            self.value -= self.source.delta
            self.source.hasEroded = True
    
    def onBranchEnd(self):
        pass
#         else:
#             print targetPoint.name
            # If the point isn't at the brink of the world then deposit
#             self._deposit(targetPoint, self.value)
         
    def branchEndCondition(self):
#         return self.source.approxFlowVelocity.length() < Vd0
        return False
     
    def calculateOutflows(self):
        direction = self.target.approxFlowVelocity if self.target.approxFlowVelocity.length() > 0 else self.target.slope
        normalizedDir = Vec3(direction)
        normalizedDir.normalize()
        
        outflows = []
        targetPoint = self.target.getPointByName(self.target.flowDirection)
        if targetPoint is None: targetPoint = self.target.getOtherPoint(self.source.name)
        
        if targetPoint.isInLake():
            # Sediment is will be later deposited on the entire lake
            basinId = targetPoint.drainageBasinId
            D.addSediment(basinId, self.value)
            
        elif targetPoint.isBedrock():
            # Check if this point drains into a lake
            basinId = targetPoint.drainageBasinId
            if basinId in D.basinsWithLakes:
                D.addSediment(basinId, self.value)
        else:
            outflowEdges = filter(lambda edge: edge.flowDirection != targetPoint.name, targetPoint.edges)
            
            sumDots = 0.
            maxDot  = 0.
            factors = []
            finalOutflowEdges = []
            for outflowEdge in outflowEdges:
    #                 if not outflowEdge.isConcave:
    #                     continue
                sdot = normalizedDir.dot(outflowEdge.getVectorStartingAt(targetPoint))
                if sdot <= 0:
                    continue
    #                 gdot = ug.dot(outflowEdge.approxFlowVelocity)
    #                 if gdot <= 0:
    #                     continue
                factors.append( sdot )
                sumDots += sdot
                maxDot   = min(max(maxDot, sdot), 1.)
                finalOutflowEdges.append(outflowEdge)
            
            for i in range(len(finalOutflowEdges)):
    #                 factor = maxDot * factors[i] / sumDots
                factor = factors[i] / sumDots
                outflows.append( SedimentEdgeFlow(factor * self.value, targetPoint, finalOutflowEdges[i]) )
            
    #             if len(targetPoint.edges) % 4 == 0:
    #                 pass
                # If the point isn't at the brink of the world then deposit
    #                 self._deposit(targetPoint, (1. - maxDot) * self.value)
            
    
        return tuple(outflows)

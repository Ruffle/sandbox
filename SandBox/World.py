'''
Created on 20/02/2016

@author: Rafal Starzyk
'''

import Tools as T
from ProcedularObjects import Section, Planet
from panda3d.core import Vec3

class World:
    '''
    classdocs
    '''

    def __init__(self, app):
        '''
        Constructor
        '''
        self.app = app
        self.coordsSystem = (T.Vector("xAxis", Vec3(10,0,0), color=Vec3(1,0,0)),
                             T.Vector("yAxis", Vec3(0,10,0), color=Vec3(0,1,0)),
                             T.Vector("zAxis", Vec3(0,0,10), color=Vec3(0,0,1)))
        self.sections = []
        self.planets = []
                
    def addSection(self, size, numIterations, law = "triangular", heightVariability = 5, roughness = 1.3, colors = T.defaultColors, randomWeights = True):
        self.sections.append( Section(self, size, numIterations, law, heightVariability, roughness, colors, randomWeights) )
        
    def addPlanet(self, radius, numIterations, law = "triangular", heightVariability = 2, roughness = 1.2, colors = T.defaultColors, randomWeights = True, numCraters = 0):
        self.planets.append( Planet(self, radius, numIterations, law, heightVariability, roughness, colors, randomWeights, numCraters) )
    
    def setShowSedimentTransport(self, boolean):
        for section in self.sections:
            section.showSedimentTransport = boolean
            section.regenVisualisation(int(self.app._slider['value']), self.app._fieldMenu.get())
    
    def setDrainageBasins(self, boolean):
        for section in self.sections:
            section.showDrainageBasins = boolean
            section.regenDrainageBasins(int(self.app._slider['value']))
    
    def setLabelType(self, type):
        for section in self.sections:
            section.labelType = type
            section.regenDrainageBasins(int(self.app._slider['value']))